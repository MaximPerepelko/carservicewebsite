package ru.carservice.util.localization;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import ru.carservice.util.SystemException;

public class DictionaryReader {
    private static long lastChangeTimestamp=System.currentTimeMillis();
    private static final Map<String,Dictionary> dictionary = new HashMap<String, Dictionary>();

	private static final String DICT_NAME = "dict.xml";
	
    static Dictionary getDictionary(String name, String locale) {
        String key = name + ":" + locale;
        
//        if(name != null) {
//        	File resource = new File(name);
//            if(!resource.exists())
//                throw new IllegalStateException("could not find dictionary: " + name);
//        }
        
        if(!dictionary.containsKey(key)) {
            Dictionary d=new Dictionary();
            fillDictionary(d,name,locale);
            dictionary.put(key, d);
        }

        return dictionary.get(key);
    }

    public static long lastChangeTimestamp() {
        return lastChangeTimestamp;
    }

    public static String localize(String dictionary, String text, String locale ) {
        return localize(dictionary,	locale, text, null);
    }
    
    public static String localize(String dictionary, String locale, String text, String textCase) {
        return getDictionary(dictionary, locale).localize(text, textCase);
    }

    static void fillDictionary(Dictionary dictionary, String name, String locale) {
        try {
            XMLReader reader= XMLReaderFactory.createXMLReader();
            reader.setContentHandler(new DictionaryContentHandler(locale,dictionary));

            
            InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
//            InputStream stream = null;
//            File resource = new File(name);
//            if(!resource.exists())
//                throw new SystemException("Resource not found: "+name);
//            stream = new FileInputStream(resource);
        
            InputSource in = new InputSource(stream);
            in.setSystemId("resource://" + name);
            reader.parse(in);
            in.getByteStream().close();
        } catch(SAXException ex) {
            throw new SystemException(ex);
        } catch(IOException ex) {
            throw new SystemException(ex);
        }
    }

}
