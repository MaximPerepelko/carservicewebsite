package ru.carservice.util;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DateUtils {
	
	private static DateFormatSymbols dateFormatSymbolsRu = new DateFormatSymbols(){

		private static final long serialVersionUID = 1L;

		@Override
        public String[] getMonths() {
            return new String[]{"Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"};
        }
        
    };
    
    private static SimpleDateFormat format = new SimpleDateFormat("dd MMMM", dateFormatSymbolsRu);
   
    public static String format(Date date) {
    	if (date != null) {
        	return format.format(date);
    	}

    	return StringUtils.EMPTY;
    }
    
    
    private static SimpleDateFormat formatFull = new SimpleDateFormat("dd MMMM yyyy", dateFormatSymbolsRu);
    
    public static String formatFull(Date date) {
    	if (date != null) {
        	return formatFull.format(date);
    	}
    	
    	return StringUtils.EMPTY;
    }
    
}
