package ru.carservice.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandExecutor {

	/**
	 * Tries to exec the command, waits for it to finsih, logs errors if exit
	 * status is nonzero, and returns true if exit status is 0 (success).
	 *
	 * @param command Description of the Parameter
	 * @return Description of the Return Value
	 */
	
	private static final Logger logger = LoggerFactory.getLogger(CommandExecutor.class);
	
	public static boolean exec(String[] command) {
		Process proc;

		try {
			System.out.println("Trying to execute command " + Arrays.asList(command));
			proc = Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			logger.error("IOException while trying to execute " + command);
			return false;
		}

		// System.out.println("Got process object, waiting to return.");

		int exitStatus = 0;

		while (true) {
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				String line = null;
				while ((line = in.readLine()) != null) {
					System.err.println(line);
				}
				in = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
				while ((line = in.readLine()) != null) {
					System.err.println(line);
				}
				exitStatus = proc.waitFor();
				break;
			} catch (java.lang.InterruptedException e) {
				logger.error("Interrupted: Ignoring and waiting", e);
			} catch (IOException ex) {
				break;
			}
		}
		if (exitStatus != 0) {
			logger.error("Error executing command: " + exitStatus);
		}
		return (exitStatus == 0);
	}
	
}
