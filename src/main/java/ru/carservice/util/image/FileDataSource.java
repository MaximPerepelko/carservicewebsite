package ru.carservice.util.image;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.carservice.config.ProjectConfiguration;

@Component
public class FileDataSource {
	
	@Autowired
	private ProjectConfiguration configuration;
//
//	public static String getImage(ProjectConfiguration configuration, UserInfo userInfo, ImageFormat format) {
//		if (userInfo != null && userInfo.getImage() != null) {
//			return getImage(configuration, userInfo.getId(), ImageDir.SOCIAL_AVATAR, format, null);
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, PageImage pageImage, ImageFormat format) {
//		if (pageImage != null) {
//			return getImage(configuration, pageImage.getId(), ImageDir.PAGE_IMAGE, format, null);
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, Settings settings, ImageFormat format) {
//		if (settings != null && settings.getImage() != null) {
//			return getImage(configuration, settings.getId(), ImageDir.SETTINGS_MENU, format, settings.getImage());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, MainImage mainImage, ImageFormat format) {
//		if (mainImage != null && mainImage.getImage() != null) {
//			return getImage(configuration, mainImage.getId(), ImageDir.MAIN_IMAGE_PHOTO, format, mainImage.getImage());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, MainImageLocalized mainImageLocalized, ImageFormat format) {
//		if (mainImageLocalized != null && mainImageLocalized.getImage() != null) {
//			return getImage(configuration, mainImageLocalized.getId(), ImageDir.MAIN_IMAGE_SLIDER, format, mainImageLocalized.getImage());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, Gallery gallery, ImageFormat format) {
//		if (gallery != null && gallery.getImage() != null) {
//			return getImage(configuration, gallery.getId(), ImageDir.GALLERY_PREVIEW, format, gallery.getImage());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, GalleryImage galleryImage, ImageFormat format) {
//		if (galleryImage != null) {
//			return getImage(configuration, galleryImage.getId(), ImageDir.GALLERY, format, null);
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, Article article, ImageFormat format) {
//		if (article != null) {
//			if (ImageFormat.ARTICLE_PREVIEW == format && article.getImage() != null) {
//				return getImage(configuration, article.getId(), ImageDir.ARTICLE_PREVIEW, format, article.getImage());
//			} else if (ImageFormat.ARTICLE_PREVIEW_SMALL == format && article.getImageSmall() != null) {
//				return getImage(configuration, article.getId(), ImageDir.ARTICLE_PREVIEW_SMALL, format, article.getImage());
//			}
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getInnerPreviewImage(ProjectConfiguration configuration, Article article, ImageFormat format) {
//		if (article != null && article.getImageInnerPreview() != null) {
//			return getImage(configuration, article.getId(), ImageDir.ARTICLE_INNER_PREVIEW, format, article.getImageInnerPreview());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, Poster poster, ImageFormat format) {
//		if (poster != null && poster.getImage() != null) {
//			return getImage(configuration, poster.getId(), ImageDir.POSTER_PREVIEW, format, poster.getImage());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, Dish dish, ImageFormat format) {
//		if (dish != null && dish.getImage() != null) {
//			return getImage(configuration, dish.getId(), ImageDir.DISH_PREVIEW, format, dish.getImage());
//		} 
//		return StringUtils.EMPTY;
//	}
//	
//	public static String getImage(ProjectConfiguration configuration, ArticleImage articleImage, ImageFormat format) {
//		if (articleImage != null) {
//			return getImage(configuration, articleImage.getId(), ImageDir.ARTICLE_CONTENT, format, null);
//		} 
//		return StringUtils.EMPTY;
//	}
//	
	public String getEntityImage(int entityId, ImageDir imageDir) {
		ImageFormat[] formats = ImageFormat.getImageFormats(imageDir, true);
		
		if (formats.length > 0) {
			return getEntityImage(entityId, imageDir, formats[0]);
		}
		
		return StringUtils.EMPTY;
	}
	
	public String getEntityImage(int entityId, ImageDir imageDir, ImageFormat format) {
		String dir = configuration.getImageDir() + "/" + imageDir.getDir() + "/";
		return dir + entityId + "/" + format.getPrefix() + ".jpg";
	}
	
	public String getImage(int entityId, Integer image, ImageDir imageDir) {
		ImageFormat[] formats = ImageFormat.getImageFormats(imageDir, true);
		
		if (formats.length > 0) {
			return getImage(entityId, image, imageDir, formats[0]);
		}
		
		return StringUtils.EMPTY;
	}
	
	public String getImage(int entityId, Integer image, ImageDir imageDir, ImageFormat format) {
		if (image != null) {
			String dir = configuration.getImageDir() + "/" + imageDir.getDir() + "/";
			return dir + entityId + "/" + format.getPrefix() + ".jpg" + (image != null ? "?" + image : "");
		}
		
		return StringUtils.EMPTY;
	}
	
}
