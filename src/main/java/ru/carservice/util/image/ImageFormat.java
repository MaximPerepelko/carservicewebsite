package ru.carservice.util.image;

import org.apache.commons.lang3.ArrayUtils;

public enum ImageFormat {
	ORIGINAL("o", 0, 0, 0, 0, false, true, false),
	CONTENT("c", 940, 0, 940, 0, false, false, false),
	CARWASH_PREVIEW("cwp", 258, 207, 258, 207, false, false, false),
	CARSERVICE_PREVIEW("csp", 258, 207, 258, 207, false, false, false),
	NEWS_PREVIEW("np", 258, 207, 258, 207, false, false, false),
	
	
	ARTICLE_PREVIEW_SMALL("aps", 283, 400, 283, 400, false, false, false),
	ARTICLE_INNER_PREVIEW("aip", 1920, 380, 1920, 380, false, false, false),
	DISH_PREVIEW("dp", 0, 1080, 1920, 0, false, false, false),
	POSTER_PREVIEW("pp", 271, 384, 271, 384, false, false, false),
	POSTER_PREVIEW_LARGE("ppl", 707, 1000, 707, 1000, false, false, false),
	GALLERY_PREVIEW("gp", 358, 250, 358, 250, false, false, false),
	GALLERY_FULL("gf", 1100, 0, 0, 700, false, true, false),
	GALLERY_FULL_SMALL("gfs", 250, 250, 250, 250, true, false, false),
	MAIN_IMAGE_SLIDER("mis", 1100, 450, 1100, 450, false, false, false),
	MAIN_IMAGE_PHOTO_PREVIEW("mipp", 550, 550, 550, 550, false, false, false),
	MAIN_IMAGE_PHOTO_FULL("mipf", 1100, 0, 0, 700, false, false, false),
	SETTINGS_MENU("sm", 1100, 0, 1100, 0, false, false, false),
	PAGE_IMAGE_PREVIEW("pip", 420, 287, 420, 287, false, false, false),
	PAGE_IMAGE_FULL("pif", 1100, 0, 0, 700, false, false, false),
	SOCIAL_AVATAR("sa", 68, 68, 68, 68, false, false, false);
	
	private String prefix;
	private int widthVertical;
	private int heightVertical;
	private int widthHorizontal;
	private int heightHorizonal;
	private boolean isSquare;
	private boolean hightQuality;
	private boolean isGray;
	
	private ImageFormat(String prefix, 
			int widthVertical, int heightVertical, 
			int widthHorizontal, int heightHorizonal,
			boolean isSquare, boolean hightQuality, boolean isGray) {
		this.prefix = prefix;
		this.widthVertical = widthVertical;
		this.heightVertical = heightVertical;
		this.widthHorizontal = widthHorizontal;
		this.heightHorizonal = heightHorizonal;
		this.isSquare = isSquare;
		this.hightQuality = hightQuality;
		this.isGray = isGray;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public int getWidthHorizontal() {
		return widthHorizontal;
	}
	
	public int getHeightHorizonal() {
		return heightHorizonal;
	}
	
	public int getWidthVertical() {
		return widthVertical;
	}
	
	public int getHeightVertical() {
		return heightVertical;
	}
	
	public boolean isSquare() {
		return isSquare;
	}
	
	public boolean isHightQuality() {
		return hightQuality;
	}
	
	public boolean isGray() {
		return isGray;
	}
	
	public void setGray(boolean isGray) {
		this.isGray = isGray;
	}
	
	public static ImageFormat[] getImageFormats(ImageDir dir) {
		return getImageFormats(dir, false);
	}
	
	public static ImageFormat[] getImageFormats(ImageDir dir, boolean excludeOriginal) {
		ImageFormat[] formats = new ImageFormat[]{};
		
		if (dir == ImageDir.CARWASH_PREVIEW) {
			formats = new ImageFormat[]{CARWASH_PREVIEW};
			
		} else if (dir == ImageDir.CARSERVICE_PREVIEW) {
			formats = new ImageFormat[]{CARSERVICE_PREVIEW};
			
		} else if (dir == ImageDir.NEWS_PREVIEW) {
			formats = new ImageFormat[]{NEWS_PREVIEW};
			
		} else if (dir == ImageDir.CARWASH_CONTENT 
				|| dir == ImageDir.CARSERVICE_CONTENT 
				|| dir == ImageDir.AEROGRAPHY 
				|| dir == ImageDir.RESTORATION 
				|| dir == ImageDir.NEWS_CONTENT 
				|| dir == ImageDir.ABOUT) {
			formats = new ImageFormat[]{CONTENT};			
			
			
			
			
		} else if(dir == ImageDir.ARTICLE_PREVIEW_SMALL) {
			formats = new ImageFormat[]{ARTICLE_PREVIEW_SMALL};
		} else if(dir == ImageDir.POSTER_PREVIEW) {
			formats = new ImageFormat[]{POSTER_PREVIEW, POSTER_PREVIEW_LARGE};

		} else if(dir == ImageDir.ARTICLE_INNER_PREVIEW) {
			formats = new ImageFormat[]{ARTICLE_INNER_PREVIEW};
		} else if(dir == ImageDir.GALLERY) {
			formats = new ImageFormat[]{GALLERY_FULL, GALLERY_FULL_SMALL};
		} else if(dir == ImageDir.GALLERY_PREVIEW) {
			formats = new ImageFormat[]{GALLERY_PREVIEW};
		} else if(dir == ImageDir.DISH_PREVIEW) {
			formats = new ImageFormat[]{DISH_PREVIEW};
		} else if(dir == ImageDir.MAIN_IMAGE_PHOTO) {
			formats = new ImageFormat[]{MAIN_IMAGE_PHOTO_PREVIEW, MAIN_IMAGE_PHOTO_FULL};
		} else if(dir == ImageDir.MAIN_IMAGE_SLIDER) {
			formats = new ImageFormat[]{MAIN_IMAGE_SLIDER};
		} else if (dir == ImageDir.SETTINGS_MENU) {
			formats = new ImageFormat[]{SETTINGS_MENU};
		} else if (dir == ImageDir.PAGE_IMAGE) {
			formats = new ImageFormat[]{PAGE_IMAGE_PREVIEW, PAGE_IMAGE_FULL};
		} else if (dir == ImageDir.SOCIAL_AVATAR) {
			formats = new ImageFormat[]{SOCIAL_AVATAR};
		}
		
		if(!excludeOriginal) {
			formats = (ImageFormat[])ArrayUtils.add(formats, ImageFormat.ORIGINAL);	
		}
		return formats;
	}
	
	public static ImageFormat[] original() {
		return new ImageFormat[]{ORIGINAL};
	}
	
	public static ImageFormat getByName(String name) {
		for(ImageFormat format : ImageFormat.values()) {
			if(format.name().equalsIgnoreCase(name)) {
				return format;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "Horizontal: " + getWidthHorizontal() + "x" + getHeightHorizonal() +
			   " Vertical: " + getWidthVertical() + "x" + getHeightVertical();
	}
	
}