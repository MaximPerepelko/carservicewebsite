package ru.carservice.util.image;

public enum ImageDir {
	
	CARWASH_PREVIEW("carwash_preview"),
	CARWASH_CONTENT("carwash_content"),
	CARSERVICE_CONTENT("carservice_content"),
	CARSERVICE_PREVIEW("carservice_preview"),
	NEWS_PREVIEW("news_preview"),
	NEWS_CONTENT("news_content"),
	AEROGRAPHY("aerography_content"),
	RESTORATION("restoration_content"),
	ABOUT("about"),
	
	ARTICLE_PREVIEW_SMALL("article_preview_small"),
	ARTICLE_INNER_PREVIEW("article_inner_preview"),
	ARTICLE_CONTENT("article_content"),
	POSTER_PREVIEW("poster_preview"),
	GALLERY("gallery"),
	GALLERY_PREVIEW("gallery_preview"),
	DISH_PREVIEW("dish_preview"),
	MAIN_IMAGE_PHOTO("main_image_photo"),
	MAIN_IMAGE_SLIDER("main_image_slider"),
	SETTINGS_MENU("settings_menu"),
	PAGE_IMAGE("page_image"),
	SOCIAL_AVATAR("social_avatar");
	
	private String dir;
	
	private ImageDir(String dir) {
		this.dir = dir;
	}
	
	public String getDir() {
		return dir;
	}
	
	public static ImageDir getDir(String name) {
		for(ImageDir dir : values()) {
			if(dir.name().equalsIgnoreCase(name)) {
				return dir;
			}
		}
		return null;
	}
	
}
