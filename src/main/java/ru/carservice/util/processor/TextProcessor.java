package ru.carservice.util.processor;

import java.util.List;

import ru.carservice.model.core.BaseEntity;
import ru.carservice.util.image.ImageDir;

public interface TextProcessor {
	
	String processImages(List<? extends BaseEntity> images, String text, ImageDir imageDir);

}
