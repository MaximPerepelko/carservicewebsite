package ru.carservice.util.processor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.carservice.model.core.BaseEntity;
import ru.carservice.util.image.FileDataSource;
import ru.carservice.util.image.ImageDir;

@Component
public class TextPrepareProcessor implements TextProcessor {

	@Autowired
	private FileDataSource fileDataSource;

	@Override
	public String processImages(List<? extends BaseEntity> images, String text, ImageDir imageDir) {
		for(BaseEntity image : images) {
			StringBuffer htmlBuff = new StringBuffer();
			
			htmlBuff.append("<div class=\"content-image\">")
					.append("<img src=\"").append(fileDataSource.getEntityImage(image.getId(), imageDir)).append("\"/>")
					.append("</div>");
			text = text.replace("#[image=" + image.getId() + "]", htmlBuff.toString());
			
			htmlBuff.delete(0, htmlBuff.length());
		}

		return text;
	}
}