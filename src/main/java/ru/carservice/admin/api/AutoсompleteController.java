package ru.carservice.admin.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/autocomplete")
public class AutoсompleteController {
	
	public static class ResponseItem {
		
		private long id;
		private String text;
		private String value;

        public ResponseItem(long id, String text) {
            this.id = id;
            this.text = text;
        }

        public ResponseItem(long id, String text, String value) {
            this.id = id;
            this.text = text;
            this.value = value;
        }

        public long getId() {
			return id;
		}
		
		public void setId(long id) {
			this.id = id;
		}
		
		public String getText() {
			return text;
		}
		
		public void setText(String text) {
			this.text = text;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
    
}
