package ru.carservice.admin.menu;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class AdminMenuBar {
	
	public AdminMenuBar() {
		this.config();
	}
	
	private List<MenuItem> items = new ArrayList<MenuItem>();
	
	public List<MenuItem> getItems() {
		return items;
	}

	private void addItem(MenuItem item) {
		items.add(item);
	}
	
	private void config() {
		MenuItem carWash = new MenuItem();
		carWash.setCaption("Мойка");
		carWash.setIcon("fa-car");
		carWash.setHref("#");
		addItem(carWash);
		
			MenuItem carWashPage = new MenuItem();
			carWashPage.setCaption("Страница");
			carWashPage.setHref("page/carwash/");
			carWash.addItem(carWashPage);
			
			MenuItem carWashService = new MenuItem();
			carWashService.setCaption("Услуги");
			carWashService.setHref("carwash-service/");
			carWash.addItem(carWashService);
			
		MenuItem carService = new MenuItem();
		carService.setCaption("Сервис");
		carService.setIcon("fa-wrench");
		carService.setHref("#");
		addItem(carService);
		
			MenuItem carServicePage = new MenuItem();
			carServicePage.setCaption("Страница");
			carServicePage.setHref("page/carservice/");
			carService.addItem(carServicePage);
			
			MenuItem carServiceService = new MenuItem();
			carServiceService.setCaption("Услуги");
			carServiceService.setHref("carservice-service/");
			carService.addItem(carServiceService);
			
			MenuItem tireService = new MenuItem();
			tireService.setCaption("Шиномонтаж");
			tireService.setHref("tireservice-service/");
			carService.addItem(tireService);
			
		MenuItem aerography = new MenuItem();
		aerography.setCaption("Аэрография");
		aerography.setIcon("fa-paint-brush");
		aerography.setHref("content-page/aerography/");
		addItem(aerography);
		
		MenuItem restoration = new MenuItem();
		restoration.setCaption("Реставрация");
		restoration.setIcon("fa-heartbeat");
		restoration.setHref("content-page/restoration/");
		addItem(restoration);
		
		MenuItem news = new MenuItem();
		news.setCaption("Новости");
		news.setIcon("fa-newspaper-o");
		news.setHref("news/");
		addItem(news);
		
		MenuItem about = new MenuItem();
		about.setCaption("О нас");
		about.setIcon("fa-file-text-o");
		about.setHref("content-page/about/");
		addItem(about);
		
		MenuItem contacts = new MenuItem();
		contacts.setCaption("Контакты");
		contacts.setIcon("fa-user");
		contacts.setHref("contacts/");
		addItem(contacts);
	}
	
}
