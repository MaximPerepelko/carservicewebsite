package ru.carservice.admin.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.PageEntity;
import ru.carservice.util.ModelBuilder;

@Controller
@RequestMapping("/admin/page/{type}")
public class AdminPageEntityController extends AdminController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView get(@PathVariable PageEntity.Type type) {
		PageEntity pageEntity = applicationService.getPageByType(type);
		
		if (pageEntity == null) {
			pageEntity = new PageEntity(type);
		}
		
		return getModel(pageEntity);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView save(PageEntity pageEntity, BindingResult result) {		
		if (validate(result, pageEntity)) {
			return getModel(applicationService.save(pageEntity));
		}
		
		return getModel(pageEntity);
	}
	
	private ModelAndView getModel(PageEntity pageEntity) {
		ModelBuilder model = new ModelBuilder("admin/pageEntity");
		model.put("pageEntity", pageEntity);
		
		return model;
	}
	
	private boolean validate(BindingResult result, PageEntity pageEntity) {
		if (StringUtils.isBlank(pageEntity.getName())) {
			result.rejectValue("name", "field.required", FIELD_REQUIRED);
		}

		return !result.hasErrors();
	}

}
