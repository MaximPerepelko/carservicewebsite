package ru.carservice.admin.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.Contacts;
import ru.carservice.util.ModelBuilder;

@Controller
@RequestMapping("/admin/contacts")
public class AdminContactsController extends AdminController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView get() {
		Contacts contacts = applicationService.getContacts();
		
		if (contacts == null) {
			contacts = new Contacts();
		}
		
		return getModel(contacts);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView save(Contacts contacts, BindingResult result) {		
		if (validate(result, contacts)) {
			return getModel(applicationService.save(contacts));
		}
		
		return getModel(contacts);
	}
	
	private ModelAndView getModel(Contacts contacts) {
		ModelBuilder model = new ModelBuilder("admin/contacts");
		model.put("contacts", contacts);
		
		return model;
	}
	
	private boolean validate(BindingResult result, Contacts contacts) {
		if (StringUtils.isBlank(contacts.getAddress())) {
			result.rejectValue("address", "field.required", FIELD_REQUIRED);
		}
		if (StringUtils.isBlank(contacts.getPhone())) {
			result.rejectValue("phone", "field.required", FIELD_REQUIRED);
		}
		if (StringUtils.isBlank(contacts.getEmail())) {
			result.rejectValue("email", "field.required", FIELD_REQUIRED);
		}
		
		return !result.hasErrors();
	}

}
