package ru.carservice.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.CarService;
import ru.carservice.model.CarServiceImage;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.Redirect;
import ru.carservice.util.TextUtils;
import ru.carservice.util.ajax.JsonResponse;
import ru.carservice.util.image.ImageDir;

@Controller
@RequestMapping("/admin/carservice-service/")
public class AdminCarServiceController extends AdminController {

    @RequestMapping(value = {"/", "/ajax"}, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables,
    						 @RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "size", required = false, defaultValue = "50") int pageSize,
                             @RequestParam(value = "id", required = false) Integer id,
                             @ModelAttribute("filter") BaseFilter filter) {
    	
        String action = req.getParameter("action");
        
        if (id != null) {
        	if("sort-down".equals(action)) {
    			CarService carService = applicationService.getCarServiceById(id);
    			applicationService.sortCarService(carService, false);
    		} else if("sort-up".equals(action)) {
    			CarService carService = applicationService.getCarServiceById(id);
    			applicationService.sortCarService(carService, true);
    		}
        	ajaxUpdate(req, res, "list");
        }

        ModelAndView modelAndView = new ModelAndView("admin/serviceList");
        if(filter == null) {        	
        	filter = new BaseFilter();
        }
        filter.addOrder(new Order("position", OrderDirection.Asc));
        
        Page<CarService> page = applicationService.findCarServices(filter, pageNumber - 1, pageSize);
        modelAndView.addObject("page", page);

        return modelAndView;
    }

    @RequestMapping(value = {"/edit/{id}", "/edit"}, method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Map<String, String> pathVariables) {
    	ModelAndView modelAndView = getModel(TextUtils.asInt(pathVariables.get("id")));
    	
        return modelAndView;
    }
    
    @RequestMapping(value = {"/edit", "/edit/{id}"}, method = RequestMethod.POST)
    public ModelAndView save(CarService carService, BindingResult result) {
        if (validate(result, carService)) {
        	if (carService.getId() != 0) {
        		CarService saved = applicationService.getCarServiceById(carService.getId());
        		saved.setName(carService.getName());
        		saved.setShortText(carService.getShortText());
        		saved.setText(carService.getText());
        		saved.setPath(carService.getPath());
        		saved.setVisible(carService.isVisible());
        		saved.setHtmlTitle(carService.getHtmlTitle());
        		saved.setMetaDescription(carService.getMetaDescription());
        		saved.setMetaKeywords(carService.getMetaDescription());
        		
        		carService = saved;
        	}
        	
        	return new Redirect("/admin/carservice-service/edit/" + applicationService.save(carService).getId() + "/");
        }

        return new ModelAndView("admin/editCarservice");
    }
    
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse remove(@PathVariable("id") int id) {
        JsonResponse response = new JsonResponse();
        try {
            applicationService.remove(new CarService(id));
        } catch(Exception e) {
            response.setStatus("error");
        }
        return response;
    }
    
    @RequestMapping(value="/edit/{id}/ajax", method=RequestMethod.POST)
	public ModelAndView onAjax(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables) {
		String action = req.getParameter("action");
		
		int id = TextUtils.asInt(pathVariables.get("id"));
		if (id != 0) {
			if ("refresh-crop-image".equals(action)) {
				ajaxUpdate(req, res, "cropImagePanel");
			} else if ("refresh-image".equals(action)) {
				ajaxUpdate(req, res, "image-list");
			}
		}	
		
		return getModel(id);
	}
    
    private ModelAndView getModel(int id) {
        ModelBuilder model = new ModelBuilder("admin/editCarservice");
        CarService carService;
        
        if (id == 0) {
        	carService = new CarService();                   
        } else {
        	carService = applicationService.getCarServiceById(id);
        	model.put("image", fileDataSource.getImage(carService.getId(), carService.getImage(), ImageDir.CARSERVICE_PREVIEW));
        	
        	for (CarServiceImage carServiceImage : applicationService.getCarServiceImages(carService)) {
        		ModelBuilder imageModel = model.createCollection("images");
        		imageModel.put("id", carServiceImage.getId());
        		imageModel.put("image", fileDataSource.getEntityImage(carServiceImage.getId(), ImageDir.CARSERVICE_CONTENT));
        	}
        }
        model.put("carService", carService);        
        
		return model;
    }
    
    private boolean validate(BindingResult result, CarService carService) {
        if (StringUtils.isBlank(carService.getName())) {
            result.rejectValue("name", "field.required", FIELD_REQUIRED);
        }
        
        return !result.hasErrors();
    }

}
