package ru.carservice.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.ContentPageEntity;
import ru.carservice.model.ContentPageEntityImage;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.image.ImageDir;

@Controller
@RequestMapping("/admin/content-page/{type}")
public class AdminContentPageEntityController extends AdminController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView get(@PathVariable ContentPageEntity.Type type) {
		ContentPageEntity contentPageEntity = applicationService.getContentPageByType(type);
		
		if (contentPageEntity == null) {
			contentPageEntity = new ContentPageEntity(type);
		}
		
		return getModel(contentPageEntity);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView save(ContentPageEntity contentPageEntity, BindingResult result) {
		if (validate(result, contentPageEntity)) {
			return getModel(applicationService.save(contentPageEntity));
		}
		
		return getModel(contentPageEntity);
	}
	
    @RequestMapping(value="/ajax", method=RequestMethod.POST)
	public ModelAndView onAjax(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables) {
		String action = req.getParameter("action");
		
		ContentPageEntity.Type type = ContentPageEntity.Type.getType(pathVariables.get("type"));
		if (type != null) {
			if ("refresh-image".equals(action)) {
				ajaxUpdate(req, res, "image-list");
			}
		}	
		ContentPageEntity contentPageEntity = applicationService.getContentPageByType(type);
		
		return getModel(contentPageEntity);
	}
	
	private ModelAndView getModel(ContentPageEntity contentPageEntity) {
		ModelBuilder model = new ModelBuilder("admin/contentPageEntity");
		model.put("contentPageEntity", contentPageEntity);
		
		if (contentPageEntity.getId() != 0) {
			for (ContentPageEntityImage contentPageEntityImage : applicationService.getContentPageEntityImages(contentPageEntity)) {
	    		ModelBuilder imageModel = model.createCollection("images");
	    		imageModel.put("id", contentPageEntityImage.getId());
	    		imageModel.put("image", fileDataSource.getEntityImage(contentPageEntityImage.getId(), ImageDir.getDir(contentPageEntity.getType().name())));
	    	}
		}	
		
		return model;
	}
	
	private boolean validate(BindingResult result, ContentPageEntity contentPageEntity) {
	  	if (StringUtils.isBlank(contentPageEntity.getName())) {
	  		result.rejectValue("name", "field.required", FIELD_REQUIRED);
	  	}

	  	return !result.hasErrors();
	}

}
