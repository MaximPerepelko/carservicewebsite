package ru.carservice.admin.controller.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ru.carservice.admin.menu.AdminMenuBar;
import ru.carservice.admin.menu.MenuItem;
import ru.carservice.config.ProjectConfiguration;
import ru.carservice.service.ApplicationService;
import ru.carservice.service.PersonServer;
import ru.carservice.service.PersonService;
import ru.carservice.util.PagerUtils;
import ru.carservice.util.ajax.AjaxUpdater;
import ru.carservice.util.image.FileDataSource;


public abstract class AdminController extends AjaxUpdater {
	
	protected static final String FIELD_REQUIRED = "Необходимо заполнить поле";

	@Autowired
	protected PersonServer personServer;
	
	@Autowired
	protected PersonService personService;
	
	@Autowired
	protected ApplicationService applicationService;
	
    @Autowired
    protected ProjectConfiguration projectConfiguration;
    
    @Autowired
    protected FileDataSource fileDataSource;
	
	@Autowired
	private AdminMenuBar adminMenuBar;
	
    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
    @ModelAttribute("menu")
    public List<MenuItem> menu() {
        return adminMenuBar.getItems();
    }

    @ModelAttribute("activeMenu")
    public MenuItem activeMenu() {
    	String currenHref = currentHref();
    	for(MenuItem item : adminMenuBar.getItems()) {
    		if(currenHref.contains(item.getHref())) {
    			return item;
    		}
    		for(MenuItem subItem : item.getItems()) {
    			if(currenHref.contains(subItem.getHref())) {
        			return subItem;
        		}
    		}
    	}
    	return null;
    }
    
    @ModelAttribute("currentHref")
    public String currentHref() {
    	return ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes())
				.getRequest().getRequestURI();
    }
	
	@ModelAttribute("pagerPath")
	private String getPagerPath(HttpServletRequest request) {
		return PagerUtils.getPagerPath(request);
	}
	
}