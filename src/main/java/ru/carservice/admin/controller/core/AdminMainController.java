package ru.carservice.admin.controller.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.model.auth.Person;
import ru.carservice.util.ModelBuilder;

@Controller
@RequestMapping("/admin")
public class AdminMainController extends AdminController {
	
	private static final String FAIL_LOGIN = "fail";
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String getMainPage() {
		Person person = personServer.getCurrentUser();
		
		if(person == null) {
			return "redirect:/admin/login";
		}
		return "admin/main";
	}

    @RequestMapping(value = "/login*", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value="status", required=false) String status) {
    	ModelBuilder model = new ModelBuilder("admin/login");
    	
    	if (StringUtils.isNotBlank(status)) {
    		if (FAIL_LOGIN.equals(status)) {
    			model.put("status", "fail");
    		}
    	}
        return model;
    }

}
