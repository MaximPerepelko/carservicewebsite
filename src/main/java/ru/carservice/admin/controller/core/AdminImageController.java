package ru.carservice.admin.controller.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.sun.media.jai.codec.SeekableStream;

import ru.carservice.config.ProjectConfiguration;
import ru.carservice.model.CarService;
import ru.carservice.model.CarServiceImage;
import ru.carservice.model.CarWash;
import ru.carservice.model.CarWashImage;
import ru.carservice.model.ContentPageEntity;
import ru.carservice.model.ContentPageEntityImage;
import ru.carservice.model.News;
import ru.carservice.model.NewsImage;
import ru.carservice.model.core.HasImage;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.ajax.JsonData;
import ru.carservice.util.ajax.JsonResponse;
import ru.carservice.util.image.Converter;
import ru.carservice.util.image.ImageDir;
import ru.carservice.util.image.ImageFormat;
import ru.carservice.util.image.ImageInfo;
import ru.carservice.util.image.UploadImageHelper;

@Controller
@RequestMapping("/")
public class AdminImageController implements InitializingBean {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminImageController.class);
	private static final String MODE_TMP_UPLOAD = "mode_tmp_upload";
	private static final String MODE_UPLOAD = "mode_upload";
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private UploadImageHelper uploadImageHelper;
	
	@Autowired
	private Converter converter;
	
	@Autowired
	private ProjectConfiguration projectConfiguration;
	
	private File uploadDir;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		uploadDir = new File(projectConfiguration.getUploadDir());
	}
	
	@RequestMapping(value="/tmpfile/{name}", method = RequestMethod.GET)
	public void getTmpImage(HttpServletResponse resp,			               
			                @PathVariable(value="name") String name) throws IOException {
		int idx = name.indexOf(".");
		if(idx > 0) {
			name = name.substring(0, idx);
		}

		InputStream isTmp = null;
		File input = new File(uploadDir, name);
		if (input.exists()) {
			try {
				isTmp = new FileInputStream(input);
			} catch (FileNotFoundException e) {}
		}
		
		if(isTmp != null) {
			resp.setHeader("Cache-Control", "public");
			final long expiration = System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 30L;
			resp.setDateHeader("Expires", expiration);
			resp.setDateHeader("Last-Modified", expiration);
			resp.setContentType("image/jpeg");
			IOUtils.copy(isTmp, resp.getOutputStream());
			return;
		}
		resp.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
	
	@RequestMapping(value="/admin/upload_image", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse uploadImage(HttpServletRequest req, 
						      @RequestParam(value="mode", required=false) String mode,
							  @RequestParam(value="imageUrl", required=false) String imageUrl) throws IOException {
		JsonResponse response = new JsonResponse();
		List<byte[]> imagesList = new ArrayList<byte[]>();
		
		MultipartFile multiPart = null;
		if(req instanceof MultipartHttpServletRequest) {
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)req;
			multiPart = multiRequest.getFile("uploadImage");
		}
		
		if(multiPart != null) {
			byte[] image = IOUtils.toByteArray(multiPart.getInputStream());
			imagesList.add(image);
		}
		if(!StringUtils.isBlank(imageUrl)) {
			byte[] image = getImage(imageUrl);
			if(image != null && image.length > 0) {
				imagesList.add(image);
			}
		}
		if(MODE_TMP_UPLOAD.equals(mode)) {
			for(byte[] image : imagesList) {
				final File tmpFile = File.createTempFile(RandomStringUtils.randomAlphabetic(5), "", uploadDir);
				try {
					if("true".equals(req.getParameter("isEncrease"))) {
						ImageInfo imageInfo = converter.getImageInfo(image);
						int size = imageInfo.getWidth() > imageInfo.getHeigth() ? imageInfo.getWidth() : imageInfo.getHeigth();
						image = converter.increaseImage(image, size, size, false);
					}
					IOUtils.write(image, new FileOutputStream(tmpFile));
					
					JsonData data = response.createJsonData();
					data.put("path", "/tmpfile/" + tmpFile.getName() + "/");
					data.put("fileName", tmpFile.getName());
				} catch(Exception e) {
					tmpFile.delete();
				}
			}
		} else if(MODE_UPLOAD.equals(mode)) {
			ImageDir dir = ImageDir.getDir(req.getParameter("type"));
			if(dir == null) {
				return response;
			}
			for(byte[] image : imagesList) {
				if(dir == ImageDir.CARWASH_CONTENT) {
					uploadCarWashContentImage(req, image);
				} else if(dir == ImageDir.CARSERVICE_CONTENT) {
					uploadCarServiceContentImage(req, image);
				} else if(dir == ImageDir.NEWS_CONTENT) {
					uploadNewsContentImage(req, image);
				} else if(dir == ImageDir.AEROGRAPHY || dir == ImageDir.RESTORATION || dir == ImageDir.ABOUT) {
					uploadContentPageEntityImage(req, image, dir);
				}
			}
		}
		response.setStatus("success");
		return response;
	}
	
	@RequestMapping(value="/admin/upload_image/save", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponse saveImage(HttpServletRequest req, 
							  @RequestParam("type") String type,
							  @RequestParam("tmpFile") String tmpFile) throws IOException {
		JsonResponse response = new JsonResponse();
		
		List<byte[]> imagesList = new ArrayList<byte[]>();
		if(!StringUtils.isBlank(tmpFile)) {
			File file = new File(uploadDir, tmpFile);
			byte[] image = getBytes(file);
			if(image != null) {
				imagesList.add(image);
			}
		}
		
		ImageDir dir = ImageDir.getDir(req.getParameter("type"));
		if(dir == null) {
			return response;
		}
		for(byte[] image : imagesList) {
			if(dir == ImageDir.CARWASH_PREVIEW) {
				uploadCarWashImage(req, image);
			} else if(dir == ImageDir.CARSERVICE_PREVIEW) {
				uploadCarServiceImage(req, image);
			} else if(dir == ImageDir.NEWS_PREVIEW) {
				uploadNewsImage(req, image);
			}
		}
		response.setStatus("success");
		return response;
	}
	
	@RequestMapping(value="/admin/upload_image/remove", method = RequestMethod.POST)
	@ResponseBody
	public void removeImage(HttpServletRequest req, 
							  @RequestParam("type") String type,
							  @RequestParam("id") Integer id) throws IOException {
		ImageDir dir = ImageDir.getDir(req.getParameter("type"));
		if(dir == null) {
			return;
		}
	
		if(dir == ImageDir.CARWASH_PREVIEW) {
			removeCarWashImage(id);
		} else if(dir == ImageDir.CARWASH_CONTENT) {
			removeCarWashContentImage(id);
		} else if(dir == ImageDir.CARSERVICE_CONTENT) {
			removeCarServiceContentImage(id);
		} else if(dir == ImageDir.CARSERVICE_PREVIEW) {
			removeCarServiceImage(id);
		} else if(dir == ImageDir.NEWS_CONTENT) {
			removeNewsContentImage(id);
		} else if(dir == ImageDir.NEWS_PREVIEW) {
			removeNewsImage(id);
		} else if(dir == ImageDir.AEROGRAPHY || dir == ImageDir.RESTORATION || dir == ImageDir.ABOUT) {
			removeContentPageEntityImage(id, dir);
		}
	}
	
	private <T extends HasImage> T uploadImage(T item, ImageDir dir, HttpServletRequest req, byte[] image) {
		int x = ServletRequestUtils.getIntParameter(req, "x", 0);
		int y = ServletRequestUtils.getIntParameter(req, "y", 0);
		int widthCrop = ServletRequestUtils.getIntParameter(req, "width", 0);
		int heightCrop = ServletRequestUtils.getIntParameter(req, "height", 0);
		
		if(item != null) {
			byte[] cropImage = converter.cropImage(image, x, y, widthCrop, heightCrop);
			
			int ts = uploadImageHelper.uploadImage(dir, ImageFormat.getImageFormats(dir, false), cropImage, item.getId());
			item.setImage(ts);
		}
		return item;
	}
	
	private void uploadImage(ImageDir dir, HttpServletRequest req, byte[] image, int id) {
		int x = ServletRequestUtils.getIntParameter(req, "x", 0);
		int y = ServletRequestUtils.getIntParameter(req, "y", 0);
		int widthCrop = ServletRequestUtils.getIntParameter(req, "width", 0);
		int heightCrop = ServletRequestUtils.getIntParameter(req, "height", 0);
		
		byte[] cropImage = converter.cropImage(image, x, y, widthCrop, heightCrop);		
		uploadImageHelper.uploadImage(dir, ImageFormat.getImageFormats(dir), cropImage, id);
	}
	
	
	private void uploadContentPageEntityImage(HttpServletRequest req, byte[] image, ImageDir dir) {
		ContentPageEntity.Type type = ContentPageEntity.Type.getType(dir.name());
				
		ContentPageEntity contentPageEntity = applicationService.getContentPageByType(type);
		if(contentPageEntity != null) {
			ContentPageEntityImage contentPageEntityImage = new ContentPageEntityImage();
			contentPageEntityImage.setContentPageEntity(contentPageEntity);
			contentPageEntityImage = applicationService.save(contentPageEntityImage);
			uploadImageHelper.uploadImage(dir, ImageFormat.getImageFormats(dir), image, contentPageEntityImage.getId());
		}
	}
	
	private void removeContentPageEntityImage(Integer id, ImageDir dir) {
		if(id != null) {
			ContentPageEntityImage contentPageEntityImage = applicationService.getContentPageEntityImageById(id);
			if(contentPageEntityImage != null) {
				applicationService.remove(contentPageEntityImage);
			}
		}
	}
	
	
	private void uploadCarWashContentImage(HttpServletRequest req, byte[] image) {
		int id = ServletRequestUtils.getIntParameter(req, "id", 0);
		
		CarWash carWash = applicationService.getCarWashById(id);
		if(carWash != null) {
			CarWashImage carWashImage = new CarWashImage();
			carWashImage.setCarWash(carWash);
			carWashImage = applicationService.save(carWashImage);
			uploadImageHelper.uploadImage(ImageDir.CARWASH_CONTENT, ImageFormat.getImageFormats(ImageDir.CARWASH_CONTENT), image, carWashImage.getId());
		}
	}
	
	private void removeCarWashContentImage(Integer id) {
		if(id != null) {
			CarWashImage carWashImage = applicationService.getCarWashImageById(id);
			if(carWashImage != null) {
				applicationService.remove(carWashImage);
			}
		}
	}

	private void uploadCarWashImage(HttpServletRequest req, byte[] image) {
		int id = ServletRequestUtils.getIntParameter(req, "id", 0);
		
		CarWash carWash = applicationService.getCarWashById(id);
		carWash = uploadImage(carWash, ImageDir.CARWASH_PREVIEW, req, image);
		applicationService.save(carWash);
	}
	
	private void removeCarWashImage(Integer id) {
		if(id != null) {
			CarWash carWash = applicationService.getCarWashById(id);
			if(carWash != null) {
				uploadImageHelper.removeImage(ImageDir.CARWASH_PREVIEW, ImageFormat.getImageFormats(ImageDir.CARWASH_PREVIEW), carWash.getId());
				carWash.setImage(null);
				applicationService.save(carWash);
			}
		}
	}
	
	private void uploadCarServiceImage(HttpServletRequest req, byte[] image) {
		int id = ServletRequestUtils.getIntParameter(req, "id", 0);
		
		CarService carService = applicationService.getCarServiceById(id);
		carService = uploadImage(carService, ImageDir.CARSERVICE_PREVIEW, req, image);
		applicationService.save(carService);
	}
	
	private void removeCarServiceImage(Integer id) {
		if(id != null) {
			CarService carService = applicationService.getCarServiceById(id);
			if(carService != null) {
				uploadImageHelper.removeImage(ImageDir.CARSERVICE_PREVIEW, ImageFormat.getImageFormats(ImageDir.CARSERVICE_PREVIEW), carService.getId());
				carService.setImage(null);
				applicationService.save(carService);
			}
		}
	}
	
	private void uploadCarServiceContentImage(HttpServletRequest req, byte[] image) {
		int id = ServletRequestUtils.getIntParameter(req, "id", 0);
		
		CarService carService = applicationService.getCarServiceById(id);
		if(carService != null) {
			CarServiceImage carServiceImage = new CarServiceImage();
			carServiceImage.setCarService(carService);
			carServiceImage = applicationService.save(carServiceImage);
			uploadImageHelper.uploadImage(ImageDir.CARSERVICE_CONTENT, ImageFormat.getImageFormats(ImageDir.CARSERVICE_CONTENT), image, carServiceImage.getId());
		}
	}
	
	private void removeCarServiceContentImage(Integer id) {
		if(id != null) {
			CarServiceImage carServiceImage = applicationService.getCarServiceImageById(id);
			if(carServiceImage != null) {
				applicationService.remove(carServiceImage);
			}
		}
	}
	
	private void uploadNewsImage(HttpServletRequest req, byte[] image) {
		int id = ServletRequestUtils.getIntParameter(req, "id", 0);
		
		News news = applicationService.getNewsById(id);
		news = uploadImage(news, ImageDir.NEWS_PREVIEW, req, image);
		applicationService.save(news);
	}
	
	private void removeNewsImage(Integer id) {
		if(id != null) {
			News news = applicationService.getNewsById(id);
			if(news != null) {
				uploadImageHelper.removeImage(ImageDir.NEWS_PREVIEW, ImageFormat.getImageFormats(ImageDir.NEWS_PREVIEW), news.getId());
				news.setImage(null);
				applicationService.save(news);
			}
		}
	}
		
	private void uploadNewsContentImage(HttpServletRequest req, byte[] image) {
		int id = ServletRequestUtils.getIntParameter(req, "id", 0);
		
		News news = applicationService.getNewsById(id);
		if(news != null) {
			NewsImage newsImage = new NewsImage();
			newsImage.setNews(news);
			newsImage = applicationService.save(newsImage);
			uploadImageHelper.uploadImage(ImageDir.NEWS_CONTENT, ImageFormat.getImageFormats(ImageDir.NEWS_CONTENT), image, newsImage.getId());
		}
	}
	
	private void removeNewsContentImage(Integer id) {
		if(id != null) {
			NewsImage newsImage = applicationService.getNewsImageById(id);
			if(newsImage != null) {
				applicationService.remove(newsImage);
			}
		}
	}
	
	
	protected byte[] getImage(String url) {
		try {
			if(url.indexOf("http") < 0) {
				url += "http://" + url;
			}
			HttpClient httpClient = new HttpClient();
			httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(10000);
			httpClient.getHttpConnectionManager().getParams().setSoTimeout(10000);
			
			GetMethod get = new GetMethod(url);
			get.addRequestHeader("Accept-Language", "ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3");
			get.addRequestHeader("Accept-Encoding", "gzip, deflate");
			get.addRequestHeader("Connection", "keep-alive");
			get.addRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:16.0) Gecko/20100101 Firefox/16.0");
			httpClient.executeMethod(get);
			
			InputStream is = get.getResponseBodyAsStream();
			RenderedOp image = JAI.create("stream", SeekableStream.wrapInputStream(is, true));
			if(image.getWidth() > 0 && image.getHeight() > 0) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
        		ImageIO.write(image.getAsBufferedImage(), "jpg", baos);
        		return baos.toByteArray();
			}
		} catch (Exception e) {
			LOGGER.error("Dont upload image by url: " + url, e);
		}
		return null;
	}
	
	protected byte[] getBytes(File output) throws IOException {
		if (output.length() > 0) {
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(output);
				ByteArrayOutputStream fos = new ByteArrayOutputStream();
				IOUtils.copy(fis, fos);
				return fos.toByteArray();
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						LOGGER.error("IO", e);
					}
				}
			}
		}
		return null;
	}
	
}
