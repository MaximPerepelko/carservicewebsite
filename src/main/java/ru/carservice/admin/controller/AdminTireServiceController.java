package ru.carservice.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.TireService;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.Redirect;
import ru.carservice.util.TextUtils;
import ru.carservice.util.ajax.JsonResponse;

@Controller
@RequestMapping("/admin/tireservice-service/")
public class AdminTireServiceController extends AdminController {

    @RequestMapping(value = {"/", "/ajax"}, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables,
    						 @RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "size", required = false, defaultValue = "50") int pageSize,
                             @RequestParam(value = "id", required = false) Integer id,
                             @ModelAttribute("filter") BaseFilter filter) {
    	
        String action = req.getParameter("action");
        
        if (id != null) {
        	if("sort-down".equals(action)) {
    			TireService tireService = applicationService.getTireServiceById(id);
    			applicationService.sortTireService(tireService, false);
    		} else if("sort-up".equals(action)) {
    			TireService tireService = applicationService.getTireServiceById(id);
    			applicationService.sortTireService(tireService, true);
    		}
        	ajaxUpdate(req, res, "list");
        }

        ModelAndView modelAndView = new ModelAndView("admin/serviceList");
        if(filter == null) {        	
        	filter = new BaseFilter();
        }
        filter.addOrder(new Order("position", OrderDirection.Asc));
        
        Page<TireService> page = applicationService.findTireServices(filter, pageNumber - 1, pageSize);
        modelAndView.addObject("page", page);

        return modelAndView;
    }

    @RequestMapping(value = {"/edit/{id}", "/edit"}, method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Map<String, String> pathVariables) {
    	ModelAndView modelAndView = getModel(TextUtils.asInt(pathVariables.get("id")));
    	
        return modelAndView;
    }
    
    @RequestMapping(value = {"/edit", "/edit/{id}"}, method = RequestMethod.POST)
    public ModelAndView save(TireService tireService, BindingResult result) {
        if (validate(result, tireService)) {
        	if (tireService.getId() != 0) {
        		TireService saved = applicationService.getTireServiceById(tireService.getId());
        		saved.setName(tireService.getName());      		
        		saved.setVisible(tireService.isVisible());
        		
        		tireService = saved;
        	}
        	
        	return new Redirect("/admin/tireservice-service/edit/" + applicationService.save(tireService).getId() + "/");
        }

        return new ModelAndView("admin/editTireservice");
    }
    
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse remove(@PathVariable("id") int id) {
        JsonResponse response = new JsonResponse();
        try {
            applicationService.remove(new TireService(id));
        } catch(Exception e) {
            response.setStatus("error");
        }
        return response;
    }

    private ModelAndView getModel(int id) {
        ModelBuilder model = new ModelBuilder("admin/editTireservice");
        TireService tireService;
        
        if (id == 0) {
        	tireService = new TireService();                   
        } else {
        	tireService = applicationService.getTireServiceById(id);
        }
        model.put("tireService", tireService);        
        
		return model;
    }
    
    private boolean validate(BindingResult result, TireService tireService) {
        if (StringUtils.isBlank(tireService.getName())) {
            result.rejectValue("name", "field.required", FIELD_REQUIRED);
        }
        
        return !result.hasErrors();
    }

}
