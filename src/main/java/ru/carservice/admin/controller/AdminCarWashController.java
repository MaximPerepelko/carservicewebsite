package ru.carservice.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.CarWash;
import ru.carservice.model.CarWashImage;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.Redirect;
import ru.carservice.util.TextUtils;
import ru.carservice.util.ajax.JsonResponse;
import ru.carservice.util.image.ImageDir;

@Controller
@RequestMapping("/admin/carwash-service/")
public class AdminCarWashController extends AdminController {

    @RequestMapping(value = {"/", "/ajax"}, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables,
    						 @RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "size", required = false, defaultValue = "50") int pageSize,
                             @RequestParam(value = "id", required = false) Integer id,
                             @ModelAttribute("filter") BaseFilter filter) {
    	
        String action = req.getParameter("action");
        
        if (id != null) {
        	if("sort-down".equals(action)) {
    			CarWash carWash = applicationService.getCarWashById(id);
    			applicationService.sortCarWash(carWash, false);
    		} else if("sort-up".equals(action)) {
    			CarWash carWash = applicationService.getCarWashById(id);
    			applicationService.sortCarWash(carWash, true);
    		}
        	ajaxUpdate(req, res, "list");
        }

        ModelAndView modelAndView = new ModelAndView("admin/serviceList");
        if(filter == null) {        	
        	filter = new BaseFilter();
        }
        filter.addOrder(new Order("position", OrderDirection.Asc));
        
        Page<CarWash> page = applicationService.findCarWashes(filter, pageNumber - 1, pageSize);
        modelAndView.addObject("page", page);

        return modelAndView;
    }

    @RequestMapping(value = {"/edit/{id}", "/edit"}, method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Map<String, String> pathVariables) {
    	ModelAndView modelAndView = getModel(TextUtils.asInt(pathVariables.get("id")));
    	
        return modelAndView;
    }
    
    @RequestMapping(value = {"/edit", "/edit/{id}"}, method = RequestMethod.POST)
    public ModelAndView save(CarWash carWash, BindingResult result) {
        if (validate(result, carWash)) {
        	if (carWash.getId() != 0) {
        		CarWash saved = applicationService.getCarWashById(carWash.getId());
        		saved.setName(carWash.getName());
        		saved.setShortText(carWash.getShortText());
        		saved.setText(carWash.getText());
        		saved.setPath(carWash.getPath());
        		saved.setVisible(carWash.isVisible());
        		saved.setHtmlTitle(carWash.getHtmlTitle());
        		saved.setMetaDescription(carWash.getMetaDescription());
        		saved.setMetaKeywords(carWash.getMetaDescription());
        		
        		carWash = saved;
        	}
        	
        	return new Redirect("/admin/carwash-service/edit/" + applicationService.save(carWash).getId() + "/");
        }

        return new ModelAndView("admin/editCarwash");
    }
    
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse remove(@PathVariable("id") int id) {
        JsonResponse response = new JsonResponse();
        try {
            applicationService.remove(new CarWash(id));
        } catch(Exception e) {
            response.setStatus("error");
        }
        return response;
    }
    
    @RequestMapping(value="/edit/{id}/ajax", method=RequestMethod.POST)
	public ModelAndView onAjax(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables) {
		String action = req.getParameter("action");
		
		int id = TextUtils.asInt(pathVariables.get("id"));
		if (id != 0) {
			if ("refresh-crop-image".equals(action)) {
				ajaxUpdate(req, res, "cropImagePanel");
			} else if ("refresh-image".equals(action)) {
				ajaxUpdate(req, res, "image-list");
			}
		}	
		
		return getModel(id);
	}
    
    private ModelAndView getModel(int id) {
        ModelBuilder model = new ModelBuilder("admin/editCarwash");
        CarWash carWash;
        
        if (id == 0) {
        	carWash = new CarWash();                   
        } else {
        	carWash = applicationService.getCarWashById(id);
        	model.put("image", fileDataSource.getImage(carWash.getId(), carWash.getImage(), ImageDir.CARWASH_PREVIEW));
        	
        	for (CarWashImage carWashImage : applicationService.getCarWashImages(carWash)) {
        		ModelBuilder imageModel = model.createCollection("images");
        		imageModel.put("id", carWashImage.getId());
        		imageModel.put("image", fileDataSource.getEntityImage(carWashImage.getId(), ImageDir.CARWASH_CONTENT));
        	}
        }
        model.put("carWash", carWash);        
        
		return model;
    }
    
    private boolean validate(BindingResult result, CarWash carWash) {
        if (StringUtils.isBlank(carWash.getName())) {
            result.rejectValue("name", "field.required", FIELD_REQUIRED);
        }
        
        return !result.hasErrors();
    }

}
