package ru.carservice.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.carservice.admin.controller.core.AdminController;
import ru.carservice.model.News;
import ru.carservice.model.NewsImage;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.Redirect;
import ru.carservice.util.TextUtils;
import ru.carservice.util.ajax.JsonResponse;
import ru.carservice.util.image.ImageDir;

@Controller
@RequestMapping("/admin/news/")
public class AdminNewsController extends AdminController {

    @RequestMapping(value = {"/", "/ajax"}, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables,
    						 @RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "size", required = false, defaultValue = "50") int pageSize,
                             @RequestParam(value = "id", required = false) Integer id,
                             @ModelAttribute("filter") BaseFilter filter) {

        ModelAndView modelAndView = new ModelAndView("admin/newsList");
        if(filter == null) {        	
        	filter = new BaseFilter();
        }
        filter.addOrder(new Order("date", OrderDirection.Desc));
        
        Page<News> page = applicationService.findNews(filter, pageNumber - 1, pageSize);
        modelAndView.addObject("page", page);

        return modelAndView;
    }

    @RequestMapping(value = {"/edit/{id}", "/edit"}, method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Map<String, String> pathVariables) {
    	ModelAndView modelAndView = getModel(TextUtils.asInt(pathVariables.get("id")));
    	
        return modelAndView;
    }
    
    @RequestMapping(value = {"/edit", "/edit/{id}"}, method = RequestMethod.POST)
    public ModelAndView save(News news, BindingResult result) {
        if (validate(result, news)) {
        	if (news.getId() != 0) {
        		News saved = applicationService.getNewsById(news.getId());
        		saved.setName(news.getName());
        		saved.setDate(news.getDate());
        		saved.setShortText(news.getShortText());
        		saved.setText(news.getText());
        		saved.setPath(news.getPath());
        		saved.setVisible(news.isVisible());
        		saved.setHtmlTitle(news.getHtmlTitle());
        		saved.setMetaDescription(news.getMetaDescription());
        		saved.setMetaKeywords(news.getMetaDescription());
        		
        		news = saved;
        	}
        	
        	return new Redirect("/admin/news/edit/" + applicationService.save(news).getId() + "/");
        }

        return new ModelAndView("admin/editNews");
    }
    
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse remove(@PathVariable("id") int id) {
        JsonResponse response = new JsonResponse();
        try {
            applicationService.remove(new News(id));
        } catch(Exception e) {
            response.setStatus("error");
        }
        return response;
    }
    
    @RequestMapping(value="/edit/{id}/ajax", method=RequestMethod.POST)
	public ModelAndView onAjax(HttpServletRequest req, HttpServletResponse res, @PathVariable Map<String, String> pathVariables) {
		String action = req.getParameter("action");
		
		int id = TextUtils.asInt(pathVariables.get("id"));
		if (id != 0) {
			if ("refresh-crop-image".equals(action)) {
				ajaxUpdate(req, res, "cropImagePanel");
			} else if ("refresh-image".equals(action)) {
				ajaxUpdate(req, res, "image-list");
			}
		}	
		
		return getModel(id);
	}
    
    private ModelAndView getModel(int id) {
        ModelBuilder model = new ModelBuilder("admin/editNews");
        News news;
        
        if (id == 0) {
        	news = new News();                   
        } else {
        	news = applicationService.getNewsById(id);
        	model.put("image", fileDataSource.getImage(news.getId(), news.getImage(), ImageDir.NEWS_PREVIEW));
        	
        	for (NewsImage newsImage : applicationService.getNewsImages(news)) {
        		ModelBuilder imageModel = model.createCollection("images");
        		imageModel.put("id", newsImage.getId());
        		imageModel.put("image", fileDataSource.getEntityImage(newsImage.getId(), ImageDir.NEWS_CONTENT));
        	}
        }
        model.put("news", news);        
        
		return model;
    }
    
    private boolean validate(BindingResult result, News news) {
        if (StringUtils.isBlank(news.getName())) {
            result.rejectValue("name", "field.required", FIELD_REQUIRED);
        }
        if (news.getDate() == null) {
            result.rejectValue("date", "field.required", FIELD_REQUIRED);
        }
        
        return !result.hasErrors();
    }

}
