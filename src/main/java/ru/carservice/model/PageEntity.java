package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.carservice.model.core.SeoEntity;

@Entity
@Table(name = "page")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PageEntity extends SeoEntity {
	
	public enum Type {
		
		carwash("Мойка"),
		carservice("Техцентр");
		
		private String title;

		private Type(String title) {
			this.title = title;
		}		
		
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
		
	}
	
	private Type type;
	
	public PageEntity() {}
	
	public PageEntity(Type type) {
		this.type = type;
		this.name = type.getTitle();
	}

	@Enumerated(EnumType.STRING)
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
}
