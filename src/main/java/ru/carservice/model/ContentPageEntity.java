package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.carservice.model.core.SeoEntity;

@Entity
@Table(name = "content_page")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContentPageEntity extends SeoEntity {
	
	public enum Type {
		
		aerography("Аэрография"),
		restoration("Реставрация"),
		about("О нас");
		
		private String title;

		private Type(String title) {
			this.title = title;
		}		
		
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
		
		public static Type getType(String name) {
			for(Type type : values()) {
				if(type.name().equalsIgnoreCase(name)) {
					return type;
				}
			}
			return null;
		}
		
	}
	
	private String text;
	private Type type;
	
	public ContentPageEntity() {}
	
	public ContentPageEntity(Type type) {
		this.type = type;
		this.name = type.getTitle();
	}

	@Enumerated(EnumType.STRING)
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	@org.hibernate.annotations.Type(type = "text")
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
}
