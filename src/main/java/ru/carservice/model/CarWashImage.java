package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ru.carservice.model.core.BaseEntity;

import org.hibernate.annotations.Cache;

@Entity
@Table(name = "carwash_image", indexes = {
		@Index(name = "carwash_image_carwash",  columnList="carwash_id")
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CarWashImage extends BaseEntity {
	
	private CarWash carWash;
	
	public CarWashImage() {
    }

    public CarWashImage(int id) {
    	this.id = id;
    }
	
	@ManyToOne(targetEntity=CarWash.class, fetch=FetchType.LAZY)
	@JoinColumn(name="carwash_id", nullable=false)
	public CarWash getCarWash() {
		return carWash;
	}
	
	public void setCarWash(CarWash carWash) {
		this.carWash = carWash;
	}
	
}
