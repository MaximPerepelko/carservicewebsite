package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.carservice.model.core.SeoEntity;

@Entity
@Table(name = "contacts")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Contacts extends SeoEntity {

	private String address;
	private String phone;
	private String phoneCarWash;
	private String email;
	private String emailCarWash;
	private String emailCarService;
	private String emailMaster;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneCarWash() {
		return phoneCarWash;
	}

	public void setPhoneCarWash(String phoneCarWash) {
		this.phoneCarWash = phoneCarWash;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailCarWash() {
		return emailCarWash;
	}

	public void setEmailCarWash(String emailCarWash) {
		this.emailCarWash = emailCarWash;
	}

	public String getEmailCarService() {
		return emailCarService;
	}

	public void setEmailCarService(String emailCarService) {
		this.emailCarService = emailCarService;
	}

	public String getEmailMaster() {
		return emailMaster;
	}

	public void setEmailMaster(String emailMaster) {
		this.emailMaster = emailMaster;
	}

}
