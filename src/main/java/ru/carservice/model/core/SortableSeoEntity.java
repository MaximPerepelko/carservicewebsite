package ru.carservice.model.core;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SortableSeoEntity extends SeoEntity implements Sortable {

	protected Integer position;
	protected boolean visible;
	
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	@Override
	public Integer getPosition() {
		return position;
	}
	
	@Override
	public void setPosition(Integer position) {
		this.position = position;
	}
    
}
