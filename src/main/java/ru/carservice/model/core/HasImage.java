package ru.carservice.model.core;

public interface HasImage {
	
	int getId();
	
	void setImage(Integer image);
	
}
