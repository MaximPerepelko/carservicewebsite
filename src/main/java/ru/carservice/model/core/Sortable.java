package ru.carservice.model.core;

public interface Sortable {
	
	Integer getPosition();
	
	void setPosition(Integer position);
	
}
