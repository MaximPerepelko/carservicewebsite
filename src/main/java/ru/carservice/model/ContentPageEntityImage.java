package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ru.carservice.model.core.BaseEntity;

import org.hibernate.annotations.Cache;

@Entity
@Table(name = "content_page_image", indexes = {
		@Index(name = "content_page_image_aerography",  columnList="content_page_id")
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContentPageEntityImage extends BaseEntity {
	
	private ContentPageEntity contentPageEntity;

	public ContentPageEntityImage() {}

    public ContentPageEntityImage(int id) {
    	this.id = id;
    }
	
	@ManyToOne(targetEntity=ContentPageEntity.class, fetch=FetchType.LAZY)
	@JoinColumn(name="content_page_id", nullable=false)	
	public ContentPageEntity getContentPageEntity() {
		return contentPageEntity;
	}

	public void setContentPageEntity(ContentPageEntity contentPageEntity) {
		this.contentPageEntity = contentPageEntity;
	}

}
