package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import ru.carservice.model.core.HasImage;
import ru.carservice.model.core.SortableSeoEntity;

@Entity
@Table(name = "carservice")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CarService extends SortableSeoEntity implements HasImage {

	private String shortText;
	private String text;
	private Integer image;

	public CarService() {}
	
	public CarService(int id) {
		this.id = id;
	}
	
	@Type(type = "text")
	public String getShortText() {
		return shortText;
	}
	
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	
	@Type(type = "text")
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Integer getImage() {
		return image;
	}

	public void setImage(Integer image) {
		this.image = image;
	}

}
