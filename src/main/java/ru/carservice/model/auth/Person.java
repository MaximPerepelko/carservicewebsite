package ru.carservice.model.auth;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.carservice.model.core.BaseEntity;


@Entity
@Table(name = "person")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Person extends BaseEntity {

	private String login;
	private String password;

    public Person() {}

    public Person(int id) {
    	setId(id);
    }

    public Person(String login) {
        this.login = login;
    }

    @Column(unique = true, nullable = false)
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

}
