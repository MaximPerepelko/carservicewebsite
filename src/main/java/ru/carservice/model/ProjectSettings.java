package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.carservice.model.core.BaseEntity;

@Entity
@Table(name = "project_settings")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectSettings extends BaseEntity {

	private String adminEmails;

	public String getAdminEmails() {
		return adminEmails;
	}

	public void setAdminEmails(String adminEmails) {
		this.adminEmails = adminEmails;
	}

}
