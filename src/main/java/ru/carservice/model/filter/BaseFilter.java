package ru.carservice.model.filter;

public class BaseFilter extends OrderFilter {

	private String path;
	private String query;
	private Boolean visible;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
    
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}