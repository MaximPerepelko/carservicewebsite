package ru.carservice.model.filter;

public enum OrderDirection {
    Asc,
    Desc
}
