package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ru.carservice.model.core.BaseEntity;

import org.hibernate.annotations.Cache;

@Entity
@Table(name = "carservice_image", indexes = {
		@Index(name = "carservice_image_carservice",  columnList="carservice_id")
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CarServiceImage extends BaseEntity {
	
	private CarService carService;

	public CarServiceImage() {}

    public CarServiceImage(int id) {
    	this.id = id;
    }
	
	@ManyToOne(targetEntity=CarService.class, fetch=FetchType.LAZY)
	@JoinColumn(name="carservice_id", nullable=false)	
	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

}
