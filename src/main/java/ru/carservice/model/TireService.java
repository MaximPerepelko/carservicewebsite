package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ru.carservice.model.core.SortableBaseEntity;


@Entity
@Table(name = "tireservice")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TireService extends SortableBaseEntity {
	
	public TireService() {}
	
	public TireService(int id) {
		this.id = id;
	}
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
