package ru.carservice.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ru.carservice.model.core.BaseEntity;

import org.hibernate.annotations.Cache;

@Entity
@Table(name = "news_image", indexes = {
		@Index(name = "news_image_news",  columnList="news_id")
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class NewsImage extends BaseEntity {
	
	private News news;

	public NewsImage() {}

    public NewsImage(int id) {
    	this.id = id;
    }
	
	@ManyToOne(targetEntity=News.class, fetch=FetchType.LAZY)
	@JoinColumn(name="news_id", nullable=false)	
	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

}
