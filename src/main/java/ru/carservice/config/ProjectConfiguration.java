package ru.carservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProjectConfiguration {

	@Value("${upload.dir}")
	private String uploadDir;
	
	@Value("${image.dir}")
	private String imageDir;
	
	@Value("${image.upload.dir}")
	private String imageUploadDir;
	
	@Value("${image.magick.dir}")
	private String imageMagickDir;
	
	public String getImageMagickDir() {
		return imageMagickDir;
	}

	public void setImageMagickDir(String imageMagickDir) {
		this.imageMagickDir = imageMagickDir;
	}

	public String getUploadDir() {
		return uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}

	public String getImageDir() {
		return imageDir;
	}
	
	public void setImageDir(String imageDir) {
		this.imageDir = imageDir;
	}
	
	public String getImageUploadDir() {
		return imageUploadDir;
	}
	
	public void setImageUploadDir(String imageUploadDir) {
		this.imageUploadDir = imageUploadDir;
	}

}
