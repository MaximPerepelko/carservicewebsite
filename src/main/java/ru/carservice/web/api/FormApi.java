package ru.carservice.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.carservice.mail.ISendMailHelper;
import ru.carservice.model.Contacts;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.ajax.JsonResponse;

@Controller
@RequestMapping("/api/form")
public class FormApi {
	
	private static final String CAR_WASH_TYPE = "carWash";
	
	@Autowired
	private ISendMailHelper sendMailHelper;
	
	@Autowired
	private ApplicationService applicationService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse form(Form form) {
        JsonResponse response = new JsonResponse();

        Contacts contacts = applicationService.getContacts();
        String sendEmail = CAR_WASH_TYPE.equals(form.getType()) ? contacts.getEmailCarWash() : contacts.getEmail();
        
        sendMailHelper.sendFormMessage(form.getName(), form.getPhone(), form.getEmail(), form.getMessage(), sendEmail);        
        
        response.setStatus("success");
        return response;
    }    
    
	public static class Form {
		
		private String name;
		private String phone;
		private String email;
		private String message;
		private String type;
		
		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public String getPhone() {
			return phone;
		}
		
		public void setPhone(String phone) {
			this.phone = phone;
		}
		
		public String getEmail() {
			return email;
		}
		
		public void setEmail(String email) {
			this.email = email;
		}
		
		public String getMessage() {
			return message;
		}
		
		public void setMessage(String message) {
			this.message = message;
		}
		
	}
	
}
