package ru.carservice.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.model.ContentPageEntity;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.ResourceNotFoundException;
import ru.carservice.util.image.ImageDir;
import ru.carservice.util.processor.TextProcessor;
import ru.carservice.web.controller.core.BaseController;

@Controller
@RequestMapping("/{type}")
public class ContentPageEntityController extends BaseController {
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private TextProcessor textProcessor;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getList(HttpServletRequest req, HttpServletResponse res, @PathVariable ContentPageEntity.Type type) {	
		ModelBuilder model = new ModelBuilder("web/contentPage");

		ContentPageEntity contentPageEntity = applicationService.getContentPageByType(type);
		if (contentPageEntity == null) throw new ResourceNotFoundException();
		
		model.put("contentPageEntity", contentPageEntity);
		model.put("contentPageText", textProcessor.processImages(applicationService.getContentPageEntityImages(contentPageEntity), contentPageEntity.getText(), ImageDir.getDir(type.name())));

		fillMetaInf(contentPageEntity, model);
		
		return model;
	}

}
