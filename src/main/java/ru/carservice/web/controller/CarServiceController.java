package ru.carservice.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.model.CarService;
import ru.carservice.model.PageEntity;
import ru.carservice.model.PageEntity.Type;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.ResourceNotFoundException;
import ru.carservice.util.image.FileDataSource;
import ru.carservice.util.image.ImageDir;
import ru.carservice.util.processor.TextProcessor;
import ru.carservice.web.controller.core.BaseController;

@Controller
@RequestMapping("/carservice")
public class CarServiceController extends BaseController {
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private FileDataSource fileDataSource;
	
	@Autowired
	private TextProcessor textProcessor;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getList(HttpServletRequest req, HttpServletResponse res) {	
		ModelBuilder model = new ModelBuilder("web/carservices");
		
		BaseFilter filter = new BaseFilter();
		filter.setVisible(true);
    	filter.addOrder(new Order("position", OrderDirection.Asc));
    	
    	for (CarService carService : applicationService.findCarServices(filter)) {
			ModelBuilder cwm = model.createCollection("carServices");
			cwm.put("carService", carService);
			cwm.put("image", fileDataSource.getImage(carService.getId(), carService.getImage(), ImageDir.CARSERVICE_PREVIEW));
		}
    	model.put("tireServices", applicationService.findTireServices(filter));
    	
		PageEntity pageEntity = applicationService.getPageByType(Type.carservice);
		model.put("pageEntity", pageEntity);
		
		fillMetaInf(pageEntity, model);
		
		return model;
	}
	
	@RequestMapping(value = "/{path}/", method = RequestMethod.GET)
	public ModelAndView getPage(HttpServletRequest req, HttpServletResponse res, @PathVariable String path) {	
		ModelBuilder model = new ModelBuilder("web/carService");
		
		CarService carService = applicationService.getCarServiceByPath(path);		
		if (carService == null || !carService.isVisible()) throw new ResourceNotFoundException();
		
		model.put("carService", carService);
		model.put("carServiceText", textProcessor.processImages(applicationService.getCarServiceImages(carService), carService.getText(), ImageDir.CARSERVICE_CONTENT));
		
		fillMetaInf(carService, model);
		
		return model;
	}
	
}
