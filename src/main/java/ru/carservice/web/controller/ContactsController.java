package ru.carservice.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.model.Contacts;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.ModelBuilder;
import ru.carservice.web.controller.core.BaseController;

@Controller
@RequestMapping("/contacts")
public class ContactsController extends BaseController {
	
	@Autowired
	private ApplicationService applicationService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getList(HttpServletRequest req, HttpServletResponse res) {	
		ModelBuilder model = new ModelBuilder("web/contacts");

		Contacts contacts = applicationService.getContacts();
		model.put("contacts", contacts);

		fillMetaInf(contacts, model);
		
		return model;
	}

}
