package ru.carservice.web.controller.core;

import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.model.core.SeoEntity;
import ru.carservice.util.PagerUtils;
import ru.carservice.util.ajax.AjaxUpdater;

public abstract class BaseController extends AjaxUpdater {
	
    @ModelAttribute("pagerPath")
    private String getPagerPath(HttpServletRequest request) {
        return PagerUtils.getPagerPath(request);
    }

    protected void fillMetaInf(SeoEntity seoEntity, ModelAndView model) {
		fillMetaInf(seoEntity.getHtmlTitle(), seoEntity.getMetaDescription(), seoEntity.getMetaKeywords(), model);
	}
    
    protected void fillMetaInf(String htmlTitle, String htmlDescription, String htmlKeywords, ModelAndView model) {
    	if (StringUtils.isNotBlank(htmlTitle)) {
			model.addObject("htmlTitle", htmlTitle);
		}
		if (StringUtils.isNotBlank(htmlDescription)) {
			model.addObject("htmlDescription", htmlDescription);
		}
		if (StringUtils.isNotBlank(htmlKeywords)) {
			model.addObject("htmlKeywords", htmlKeywords);
		}
    }
    
    @ModelAttribute("currentHref")
    protected String currentRoot(HttpServletRequest req, ModelAndView model) {
    	StringBuffer buf = new StringBuffer();
    	for(Map.Entry<String, String[]> entry : req.getParameterMap().entrySet()) {
    		buf.append(buf.length() == 0 ? "?" : "&");
    		buf.append(entry.getKey() + "=" + entry.getValue()[0]);
    	}
		return req.getRequestURI() + buf.toString();
	}
    
    @ModelAttribute("year")
    protected int year(HttpServletRequest request) {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.YEAR);
    }

}
