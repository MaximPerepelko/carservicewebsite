package ru.carservice.web.controller.core;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.util.ModelBuilder;


@Controller
public class Page404 extends BaseController {

	@RequestMapping(value="/error404")
	public ModelAndView getPage() {
		ModelBuilder model = new ModelBuilder("web/error404");
		fillMetaInf("Legendas - 404", null, null, model);
		
		return model;
	}
	
}
