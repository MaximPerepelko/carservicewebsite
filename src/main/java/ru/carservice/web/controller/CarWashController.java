package ru.carservice.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.model.CarWash;
import ru.carservice.model.PageEntity;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.ModelBuilder;
import ru.carservice.util.ResourceNotFoundException;
import ru.carservice.util.image.FileDataSource;
import ru.carservice.util.image.ImageDir;
import ru.carservice.util.processor.TextProcessor;
import ru.carservice.web.controller.core.BaseController;

@Controller
@RequestMapping("/carwash")
public class CarWashController extends BaseController {
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private FileDataSource fileDataSource;
	
	@Autowired
	private TextProcessor textProcessor;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getList(HttpServletRequest req, HttpServletResponse res) {	
		ModelBuilder model = new ModelBuilder("web/carwashes");
		
		BaseFilter filter = new BaseFilter();
		filter.setVisible(true);
    	filter.addOrder(new Order("position", OrderDirection.Asc));
    	
		for (CarWash carWash : applicationService.findCarWashes(filter)) {
			ModelBuilder cwm = model.createCollection("carWashes");
			cwm.put("carWash", carWash);
			cwm.put("image", fileDataSource.getImage(carWash.getId(), carWash.getImage(), ImageDir.CARWASH_PREVIEW));
		}
		
		PageEntity pageEntity = applicationService.getPageByType(PageEntity.Type.carwash);
		model.put("pageEntity", pageEntity);
		
		fillMetaInf(pageEntity, model);
		
		return model;
	}
	
	@RequestMapping(value = "/{path}/", method = RequestMethod.GET)
	public ModelAndView getPage(HttpServletRequest req, HttpServletResponse res, @PathVariable String path) {	
		ModelBuilder model = new ModelBuilder("web/carWash");
		
		CarWash carWash = applicationService.getCarWashByPath(path);		
		if (carWash == null || !carWash.isVisible()) throw new ResourceNotFoundException();
		
		model.put("carWash", carWash);
		model.put("carWashText", textProcessor.processImages(applicationService.getCarWashImages(carWash), carWash.getText(), ImageDir.CARWASH_CONTENT));
		
		fillMetaInf(carWash, model);
		
		return model;
	}
	
}
