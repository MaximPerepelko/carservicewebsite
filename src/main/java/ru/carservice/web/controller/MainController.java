package ru.carservice.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.carservice.util.ModelBuilder;
import ru.carservice.web.controller.core.BaseController;

@Controller
@RequestMapping(value = "/")
public class MainController extends BaseController {

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.HEAD})
    public ModelAndView main() {
    	ModelBuilder model = new ModelBuilder("web/main");
    	model.put("isMain", true);
    	
    	return model;
    }
    
}
