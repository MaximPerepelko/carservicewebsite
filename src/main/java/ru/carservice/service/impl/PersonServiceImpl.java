package ru.carservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.carservice.model.auth.Person;
import ru.carservice.repository.auth.PersonRepository;
import ru.carservice.service.PersonService;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;
    
    @Override
    public Person save(Person person) {
        return personRepository.save(person);
    }

    @Override
    public void remove(Person person) {
        personRepository.delete(person);
    }

    @Override
    public Person findById(Person person) {
        return personRepository.findOne(person.getId());
    }

    @Override
    public Person findByLogin(Person person) {
        return personRepository.findByLogin(person.getLogin());
    }
    
}
