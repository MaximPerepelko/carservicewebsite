package ru.carservice.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.carservice.model.CarService;
import ru.carservice.model.CarServiceImage;
import ru.carservice.model.CarWash;
import ru.carservice.model.CarWashImage;
import ru.carservice.model.Contacts;
import ru.carservice.model.ContentPageEntity;
import ru.carservice.model.ContentPageEntityImage;
import ru.carservice.model.News;
import ru.carservice.model.NewsImage;
import ru.carservice.model.PageEntity;
import ru.carservice.model.ProjectSettings;
import ru.carservice.model.TireService;
import ru.carservice.model.filter.BaseFilter;
import ru.carservice.model.filter.Order;
import ru.carservice.model.filter.OrderDirection;
import ru.carservice.repository.CarServiceImageRepository;
import ru.carservice.repository.CarServiceRepository;
import ru.carservice.repository.CarWashImageRepository;
import ru.carservice.repository.CarWashRepository;
import ru.carservice.repository.ContactsRepository;
import ru.carservice.repository.ContentPageEntityImageRepository;
import ru.carservice.repository.ContentPageEntityRepository;
import ru.carservice.repository.NewsImageRepository;
import ru.carservice.repository.NewsRepository;
import ru.carservice.repository.PageEntityRepository;
import ru.carservice.repository.ProjectSettingsRepository;
import ru.carservice.repository.TireServiceRepository;
import ru.carservice.repository.spec.CarServiceSpec;
import ru.carservice.repository.spec.CarWashSpec;
import ru.carservice.repository.spec.NewsSpec;
import ru.carservice.repository.spec.TireServiceSpec;
import ru.carservice.service.ApplicationService;
import ru.carservice.util.PageRequestBuilder;
import ru.carservice.util.TranslitHelper;
import ru.carservice.util.image.ImageDir;
import ru.carservice.util.image.ImageFormat;
import ru.carservice.util.image.UploadImageHelper;

@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private PageEntityRepository pageEntityRepository;
	
	@Autowired
	private ContentPageEntityRepository contentPageEntityRepository;
	
	@Autowired
	private ContentPageEntityImageRepository contentPageEntityImageRepository;
	
	@Autowired
	private ContactsRepository contactsRepository;
	
	@Autowired
	private ProjectSettingsRepository projectSettingsRepository;
	
	@Autowired
	private CarWashRepository carWashRepository;
	
	@Autowired
	private CarWashImageRepository carWashImageRepository;
	
	@Autowired
	private CarServiceRepository carServiceRepository;
	
	@Autowired
	private CarServiceImageRepository carServiceImageRepository;
	
	@Autowired
	private TireServiceRepository tireServiceRepository;
	
	@Autowired
	private NewsRepository newsRepository;
	
	@Autowired
	private NewsImageRepository newsImageRepository;
	
	@Autowired
	private UploadImageHelper uploadImageHelper;
	
	
	@Override
	public PageEntity save(PageEntity pageEntity) {
		return pageEntityRepository.save(pageEntity);
	}
	
	@Override
	public PageEntity getPageByType(PageEntity.Type type) {
		return pageEntityRepository.findByType(type);
	}
	
	
	@Override
	public ContentPageEntity save(ContentPageEntity contentPageEntity) {
		return contentPageEntityRepository.save(contentPageEntity);
	}
	
	@Override
	public ContentPageEntity getContentPageByType(ContentPageEntity.Type type) {
		return contentPageEntityRepository.findByType(type);
	}
	
	
	@Override
	public ContentPageEntityImage getContentPageEntityImageById(int id) {
		return contentPageEntityImageRepository.findOne(id);
	}

	@Override
	public void remove(ContentPageEntityImage contentPageEntityImage) {
		String dirName = contentPageEntityImage.getContentPageEntity().getType().name();
		ImageDir dir = ImageDir.getDir(dirName);
		
		uploadImageHelper.removeImage(dir, ImageFormat.getImageFormats(dir), contentPageEntityImage.getId());
		contentPageEntityImageRepository.delete(contentPageEntityImage);
	}

	@Override
	public ContentPageEntityImage save(ContentPageEntityImage contentPageEntityImage) {
		return contentPageEntityImageRepository.save(contentPageEntityImage);
	}

	@Override
	public List<ContentPageEntityImage> getContentPageEntityImages(ContentPageEntity contentPageEntity) {
		return contentPageEntityImageRepository.findByContentPageEntity(contentPageEntity);
	}
	
	
	@Override
	public Contacts save(Contacts contacts) {
		return contactsRepository.save(contacts);
	}
	
	@Override
	public Contacts getContacts() {
		List<Contacts> contacts = contactsRepository.findAll();		
		if (!contacts.isEmpty()) {
			return contacts.get(0);
		}		
		
		return null;
	}
	
	
	@Override
    public ProjectSettings save(ProjectSettings projectSettings) {
        return projectSettingsRepository.save(projectSettings);
    }
	
	 @Override
    public ProjectSettings getProjectSettings() {
		List<ProjectSettings> projectSettings = projectSettingsRepository.findAll();
        if (!projectSettings.isEmpty()) {
        	return projectSettings.get(0);
        }

        return null;
    }
	 
	
	@Override
    public CarWash save(CarWash carWash) {
		String path = carWash.getPath();
		
		if(StringUtils.isBlank(path)) {            	
			path = TranslitHelper.traslit(carWash.getName());
		}
		
		CarWash exists = getCarWashByPath(path);
		if (exists != null && exists.getId() != carWash.getId()) {
			carWash = carWashRepository.save(carWash);
			path += "_" + carWash.getId();
		}
		carWash.setPath(path);
		
        return carWashRepository.save(carWash);
    }

    @Override
    public void remove(CarWash carWash) {
    	for(CarWashImage carWashImage : getCarWashImages(carWash)) {
    		remove(carWashImage);
    	}

    	uploadImageHelper.removeImage(ImageDir.CARWASH_PREVIEW, ImageFormat.getImageFormats(ImageDir.CARWASH_PREVIEW), carWash.getId());
    	carWashRepository.delete(carWash);
    }

    @Override
    public CarWash getCarWashById(int id) {
        return carWashRepository.findOne(id);
    }
    
    @Override
    public CarWash getCarWashByPath(String path) {
        return carWashRepository.getByPath(path);
    }
    
    @Override
    public CarWash findCarWash(BaseFilter filter) {
        return carWashRepository.findOne(CarWashSpec.filter(filter));
    }

    @Override
    public Page<CarWash> findCarWashes(BaseFilter filter, int pageNumber, int pageSize) {
        return carWashRepository.findAll(CarWashSpec.filter(filter), PageRequestBuilder.build(filter, pageNumber, pageSize));
    }
    
    @Override
    public List<CarWash> findCarWashes(BaseFilter filter) {
        return carWashRepository.findAll(CarWashSpec.filter(filter), PageRequestBuilder.getSort(filter));
    }
    
	@Override
	public void sortCarWash(final CarWash carWash, final boolean isUp) {
		BaseFilter filter = new BaseFilter();
		filter.addOrder(new Order("position", OrderDirection.Asc));
		List<CarWash> carWashes = findCarWashes(filter);
		
		for(int i = 0; i < carWashes.size(); i++) {
			CarWash cw = carWashes.get(i);
			if(cw.getId() == carWash.getId()) {
				if(isUp && i > 0) {
					CarWash prev = carWashes.get(i - 1);
					carWashes.set(i - 1, cw);
					carWashes.set(i, prev);
				} else if( i + 1 < carWashes.size() ) {
					CarWash next = carWashes.get(i + 1);
					carWashes.set(i + 1, cw);
					carWashes.set(i, next);
				}
				break;
			}
		}
		
		int i = 0;
		for(CarWash pi : carWashes) {
			pi.setPosition(i++);
		}
	}
    
    
    @Override
	public CarWashImage getCarWashImageById(int id) {
		return carWashImageRepository.findOne(id);
	}

	@Override
	public void remove(CarWashImage carWashImage) {
		uploadImageHelper.removeImage(ImageDir.CARWASH_CONTENT, ImageFormat.getImageFormats(ImageDir.CARWASH_CONTENT), carWashImage.getId());
		carWashImageRepository.delete(carWashImage);
	}

	@Override
	public CarWashImage save(CarWashImage carWashImage) {
		return carWashImageRepository.save(carWashImage);
	}

	@Override
	public List<CarWashImage> getCarWashImages(CarWash carWash) {
		return carWashImageRepository.findByCarWash(carWash);
	}
	
	
	@Override
    public CarService save(CarService carService) {
		String path = carService.getPath();
		
		if(StringUtils.isBlank(path)) {            	
			path = TranslitHelper.traslit(carService.getName());
		}
		
		CarService exists = getCarServiceByPath(path);
		if (exists != null && exists.getId() != carService.getId()) {
			carService = carServiceRepository.save(carService);
			path += "_" + carService.getId();
		}
		carService.setPath(path);
		
        return carServiceRepository.save(carService);
    }

    @Override
    public void remove(CarService carService) {
    	for(CarServiceImage carServiceImage : getCarServiceImages(carService)) {
    		remove(carServiceImage);
    	}

    	uploadImageHelper.removeImage(ImageDir.CARSERVICE_PREVIEW, ImageFormat.getImageFormats(ImageDir.CARSERVICE_PREVIEW), carService.getId());
    	carServiceRepository.delete(carService);
    }

    @Override
    public CarService getCarServiceById(int id) {
        return carServiceRepository.findOne(id);
    }
    
    @Override
    public CarService getCarServiceByPath(String path) {
        return carServiceRepository.getByPath(path);
    }
    
    @Override
    public CarService findCarService(BaseFilter filter) {
        return carServiceRepository.findOne(CarServiceSpec.filter(filter));
    }

    @Override
    public Page<CarService> findCarServices(BaseFilter filter, int pageNumber, int pageSize) {
        return carServiceRepository.findAll(CarServiceSpec.filter(filter), PageRequestBuilder.build(filter, pageNumber, pageSize));
    }
    
    @Override
    public List<CarService> findCarServices(BaseFilter filter) {
        return carServiceRepository.findAll(CarServiceSpec.filter(filter), PageRequestBuilder.getSort(filter));
    }
    
	@Override
	public void sortCarService(final CarService carService, final boolean isUp) {
		BaseFilter filter = new BaseFilter();
		filter.addOrder(new Order("position", OrderDirection.Asc));
		List<CarService> carServices = findCarServices(filter);
		
		for(int i = 0; i < carServices.size(); i++) {
			CarService cw = carServices.get(i);
			if(cw.getId() == carService.getId()) {
				if(isUp && i > 0) {
					CarService prev = carServices.get(i - 1);
					carServices.set(i - 1, cw);
					carServices.set(i, prev);
				} else if( i + 1 < carServices.size() ) {
					CarService next = carServices.get(i + 1);
					carServices.set(i + 1, cw);
					carServices.set(i, next);
				}
				break;
			}
		}
		
		int i = 0;
		for(CarService pi : carServices) {
			pi.setPosition(i++);
		}
	}
    
    
    @Override
	public CarServiceImage getCarServiceImageById(int id) {
		return carServiceImageRepository.findOne(id);
	}

	@Override
	public void remove(CarServiceImage carServiceImage) {
		uploadImageHelper.removeImage(ImageDir.CARSERVICE_CONTENT, ImageFormat.getImageFormats(ImageDir.CARSERVICE_CONTENT), carServiceImage.getId());
		carServiceImageRepository.delete(carServiceImage);
	}

	@Override
	public CarServiceImage save(CarServiceImage carWashImage) {
		return carServiceImageRepository.save(carWashImage);
	}

	@Override
	public List<CarServiceImage> getCarServiceImages(CarService carService) {
		return carServiceImageRepository.findByCarService(carService);
	}
	
	
	@Override
    public TireService save(TireService tireService) {
        return tireServiceRepository.save(tireService);
    }

    @Override
    public void remove(TireService tireService) {
    	tireServiceRepository.delete(tireService);
    }

    @Override
    public TireService getTireServiceById(int id) {
        return tireServiceRepository.findOne(id);
    }
    
    @Override
    public TireService findTireService(BaseFilter filter) {
        return tireServiceRepository.findOne(TireServiceSpec.filter(filter));
    }

    @Override
    public Page<TireService> findTireServices(BaseFilter filter, int pageNumber, int pageSize) {
        return tireServiceRepository.findAll(TireServiceSpec.filter(filter), PageRequestBuilder.build(filter, pageNumber, pageSize));
    }
    
    @Override
    public List<TireService> findTireServices(BaseFilter filter) {
        return tireServiceRepository.findAll(TireServiceSpec.filter(filter), PageRequestBuilder.getSort(filter));
    }
    
	@Override
	public void sortTireService(final TireService tireService, final boolean isUp) {
		BaseFilter filter = new BaseFilter();
		filter.addOrder(new Order("position", OrderDirection.Asc));
		List<TireService> tireServices = findTireServices(filter);
		
		for(int i = 0; i < tireServices.size(); i++) {
			TireService cw = tireServices.get(i);
			if(cw.getId() == tireService.getId()) {
				if(isUp && i > 0) {
					TireService prev = tireServices.get(i - 1);
					tireServices.set(i - 1, cw);
					tireServices.set(i, prev);
				} else if( i + 1 < tireServices.size() ) {
					TireService next = tireServices.get(i + 1);
					tireServices.set(i + 1, cw);
					tireServices.set(i, next);
				}
				break;
			}
		}
		
		int i = 0;
		for(TireService pi : tireServices) {
			pi.setPosition(i++);
		}
	}
	
	
	@Override
    public News save(News news) {
		String path = news.getPath();
		
		if(StringUtils.isBlank(path)) {            	
			path = TranslitHelper.traslit(news.getName());
		}
		
		News exists = getNewsByPath(path);
		if (exists != null && exists.getId() != news.getId()) {
			news = newsRepository.save(news);
			path += "_" + news.getId();
		}
		news.setPath(path);
		
        return newsRepository.save(news);
    }

    @Override
    public void remove(News news) {
    	for(NewsImage newsImage : getNewsImages(news)) {
    		remove(newsImage);
    	}

    	uploadImageHelper.removeImage(ImageDir.NEWS_PREVIEW, ImageFormat.getImageFormats(ImageDir.CARSERVICE_PREVIEW), news.getId());
    	newsRepository.delete(news);
    }

    @Override
    public News getNewsById(int id) {
        return newsRepository.findOne(id);
    }
    
    @Override
    public News getNewsByPath(String path) {
        return newsRepository.getByPath(path);
    }
    
    @Override
    public News findNewsOne(BaseFilter filter) {
        return newsRepository.findOne(NewsSpec.filter(filter));
    }

    @Override
    public Page<News> findNews(BaseFilter filter, int pageNumber, int pageSize) {
        return newsRepository.findAll(NewsSpec.filter(filter), PageRequestBuilder.build(filter, pageNumber, pageSize));
    }
    
    @Override
    public List<News> findNews(BaseFilter filter) {
        return newsRepository.findAll(NewsSpec.filter(filter), PageRequestBuilder.getSort(filter));
    }
    
    
    @Override
	public NewsImage getNewsImageById(int id) {
		return newsImageRepository.findOne(id);
	}

	@Override
	public void remove(NewsImage newsImage) {
		uploadImageHelper.removeImage(ImageDir.NEWS_CONTENT, ImageFormat.getImageFormats(ImageDir.NEWS_CONTENT), newsImage.getId());
		newsImageRepository.delete(newsImage);
	}

	@Override
	public NewsImage save(NewsImage newsImage) {
		return newsImageRepository.save(newsImage);
	}

	@Override
	public List<NewsImage> getNewsImages(News news) {
		return newsImageRepository.findByNews(news);
	}
	
}
