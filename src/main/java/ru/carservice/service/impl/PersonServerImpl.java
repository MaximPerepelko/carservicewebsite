package ru.carservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ru.carservice.auth.AuthPerson;
import ru.carservice.model.auth.Person;
import ru.carservice.service.PersonServer;
import ru.carservice.service.PersonService;


@Service
public class PersonServerImpl implements PersonServer {
	
	@Autowired
	private PersonService personService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Person person = personService.findByLogin(new Person(username.toLowerCase()));

        if(person == null) throw new UsernameNotFoundException("User " + username + " not found");
        return new AuthPerson(person);
	}
	
	@Override
	public Person getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if((auth instanceof UsernamePasswordAuthenticationToken || auth instanceof RememberMeAuthenticationToken) 
															    && auth.isAuthenticated()) {
			Person person = ((AuthPerson)auth.getPrincipal()).getPerson();
			return person;
		}
		return null;
	}

}
