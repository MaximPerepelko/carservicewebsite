package ru.carservice.service;

import ru.carservice.model.auth.Person;

public interface PersonService {

    Person save(Person person);

    void remove(Person person);

    Person findById(Person person);

    Person findByLogin(Person person);
    
}
