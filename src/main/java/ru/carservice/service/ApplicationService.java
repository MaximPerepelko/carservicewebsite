package ru.carservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ru.carservice.model.CarService;
import ru.carservice.model.CarServiceImage;
import ru.carservice.model.CarWash;
import ru.carservice.model.CarWashImage;
import ru.carservice.model.Contacts;
import ru.carservice.model.ContentPageEntity;
import ru.carservice.model.ContentPageEntityImage;
import ru.carservice.model.News;
import ru.carservice.model.NewsImage;
import ru.carservice.model.PageEntity;
import ru.carservice.model.ProjectSettings;
import ru.carservice.model.TireService;
import ru.carservice.model.filter.BaseFilter;

public interface ApplicationService {

	PageEntity save(PageEntity pageEntity);

	PageEntity getPageByType(PageEntity.Type type);
	
	
	ContentPageEntity save(ContentPageEntity contentPageEntity);

	ContentPageEntity getContentPageByType(ContentPageEntity.Type type);
	

	ContentPageEntityImage getContentPageEntityImageById(int id);
	
	void remove(ContentPageEntityImage contentPageEntityImage);
	
	ContentPageEntityImage save(ContentPageEntityImage contentPageEntityImage);

	List<ContentPageEntityImage> getContentPageEntityImages(ContentPageEntity contentPageEntity);
	
	
	Contacts save(Contacts contacts);

	Contacts getContacts();
	

	CarWash save(CarWash carWash);

	void remove(CarWash carWash);

	CarWash getCarWashById(int id);

	CarWash getCarWashByPath(String path);

	CarWash findCarWash(BaseFilter filter);

	Page<CarWash> findCarWashes(BaseFilter filter, int pageNumber, int pageSize);

	List<CarWash> findCarWashes(BaseFilter filter);

	void sortCarWash(CarWash carWash, boolean isUp);
	

	CarWashImage getCarWashImageById(int id);

	void remove(CarWashImage carWashImage);

	CarWashImage save(CarWashImage carWashImage);

	List<CarWashImage> getCarWashImages(CarWash carWash);
	

	CarService save(CarService carService);

	void remove(CarService carService);

	CarService getCarServiceById(int id);

	CarService getCarServiceByPath(String path);

	CarService findCarService(BaseFilter filter);

	Page<CarService> findCarServices(BaseFilter filter, int pageNumber, int pageSize);

	List<CarService> findCarServices(BaseFilter filter);

	void sortCarService(CarService carService, boolean isUp);
	

	CarServiceImage getCarServiceImageById(int id);

	void remove(CarServiceImage carServiceImage);

	CarServiceImage save(CarServiceImage carWashImage);

	List<CarServiceImage> getCarServiceImages(CarService carService);
	

	TireService save(TireService tireService);

	void remove(TireService tireService);

	TireService getTireServiceById(int id);

	TireService findTireService(BaseFilter filter);

	Page<TireService> findTireServices(BaseFilter filter, int pageNumber, int pageSize);

	List<TireService> findTireServices(BaseFilter filter);

	void sortTireService(TireService tireService, boolean isUp);
	

	News save(News news);

	void remove(News news);

	News getNewsById(int id);

	News getNewsByPath(String path);

	List<News> findNews(BaseFilter filter);

	Page<News> findNews(BaseFilter filter, int pageNumber, int pageSize);

	News findNewsOne(BaseFilter filter);
	

	NewsImage getNewsImageById(int id);

	void remove(NewsImage newsImage);

	NewsImage save(NewsImage newsImage);

	List<NewsImage> getNewsImages(News news);
	

	ProjectSettings save(ProjectSettings projectSettings);

	ProjectSettings getProjectSettings();

}
