package ru.carservice.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.carservice.model.auth.Person;


public interface PersonServer extends UserDetailsService {

	Person getCurrentUser();
	
}


