
package ru.carservice.mail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.list.SynchronizedList;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

import ru.carservice.service.ApplicationService;
import ru.carservice.util.localization.Dictionary;

@ImportResource("classpath:mail-templates.xml")
public class SendMailHelper implements ISendMailHelper, InitializingBean, DisposableBean {
	
	private static final Logger LOG = LoggerFactory.getLogger(SendMailHelper.class);
	
	private static final String LOCALE ="ru";
	
	@Autowired
	private IMailService mailService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Value("mail-templates.xml")
	private String mailTemplates;
	
	private List<String> adminEmails;
	
	@SuppressWarnings("unchecked")
	private List<MailTask> tasks = SynchronizedList.decorate(new ArrayList<MailTask>());
	private SendMailThread thread;
	private boolean isShutdown;
	
	private class SendMailThread extends Thread {
		
		@Override
		public void run() {
			while(!isShutdown) {
				try{
					if(tasks.size() > 0) {
						sendMail(tasks.remove(0));
					} else {
						synchronized (this) {
							this.wait();
						}
					}
				
				} catch (Exception e) {
					LOG.error("Error send notification: ", e);
				}
			}
		}
		
	}
	
	private void addSendMailTask(MailTask task) {
		synchronized (thread) {
			tasks.add(task);
			thread.notify();
		}
	}
	
	private void sendMail(MailTask task) {
		if(task.getEmails() != null) {
			int i = 0;
			Collection<String> emails = task.getEmails();
			LOG.info("Start send mails " + emails.size());
			for(String email : emails) {
				String subject = task.getSubject();
				String body = task.getBody();
				mailService.sendHTML(email, subject, body);
				
				if(i % 100 == 0) {
					LOG.info("Send mails " +  i + " of " + emails.size());
				}
				i++;
			}
			LOG.info("End send mails " + emails.size());
		}
	}

	@Override
	public void sendFormMessage(String name, String phone, String email, String message, String sendEmail) {
		Map<String, String> replaceMap = new HashMap<String, String>();

		replaceMap.put("<NAME/>", name);
		replaceMap.put("<PHONE/>", phone);
		replaceMap.put("<EMAIL/>", email);
		replaceMap.put("<MESSAGE/>", message);
	
		String subject = replaceVariables(getSubject("form-submit"), replaceMap);
		String mail = replaceVariables(getMail("form-submit"), replaceMap);
		
		List<String> toEmails = new ArrayList<String>();
		toEmails.add(sendEmail);
		
		MailTask task = new MailTask();
		task.setSubject(subject);
		task.setBody(mail);
		task.setEmails(toEmails);
		addSendMailTask(task);
	}
	
	
	@Override
	public void afterPropertiesSet() throws Exception {		
		thread = new SendMailThread();
		thread.start();
	}
	
	@Override
	public void destroy() throws Exception {
		isShutdown = true;
		synchronized (tasks) {
			tasks.notify();
		}
	}
	
	private String getMail(String item) {
		Dictionary templates = new Dictionary();
		templates.init(mailTemplates, LOCALE);
		return templates.localize(item + "-mail", null);
	}
	
	private String getSubject(String item) {
		Dictionary templates = new Dictionary();
		templates.init(mailTemplates, LOCALE);
		return templates.localize(item + "-subject", null);
	}
	
	private String replaceVariables(String mail, Map<String, String> replaceMap) {
		for (Map.Entry<String, String> entry : replaceMap.entrySet()) {
			String value = entry.getValue() != null ? entry.getValue() : "";
			mail = mail.replace(entry.getKey(), value);
		}
		return mail;
	}

	private List<String> getAdminEmails() {
		List<String> adminEmails = new ArrayList<String>();
		String emails = applicationService.getProjectSettings().getAdminEmails();
		
		if (StringUtils.isNotBlank(emails)) {
			for (String email : emails.split(",")) {
				adminEmails.add(email);
			}
		}
		return adminEmails;
	}
	
	@Value("${mail.admin.emails}")
	public void setAdminEmails(String adminEmails) {
		this.adminEmails = new ArrayList<String>();
		for(String email : adminEmails.split(",")) {
			this.adminEmails.add(email);
		}
	}


}
