package ru.carservice.mail;

import java.util.Collection;
import java.util.HashSet;

public class MailTask {

	private Collection<String> emails;
	private String subject;
	private String body;
	
	public Collection<String> getEmails() {
		return emails;
	}
	
	public void setEmails(Collection<String> emails) {
		this.emails = emails;
	}
	
	public void addEmail(String email) {
		if(emails == null) {
			emails = new HashSet<String>();
		}
		emails.add(email);
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
}
