package ru.carservice.mail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MailService implements IMailService, InitializingBean {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);
	
	@Value("${mail.smtp.host}")
	private String smtpHost;

	@Value("${mail.smtp.user}")
	private String smtpUser;
	
	@Value("${mail.smtp.password}")
	private String smtpPassword;
	
	@Value("${mail.smtp.from-address}")
	private String fromAddress;
	
	@Value("${mail.smtp.from-person}")
	private String fromPerson;
	
	private Session session;
	private int smtpPort = 465;
	private InternetAddress internetAddress;
	
	public void afterPropertiesSet() throws Exception {
		if (StringUtils.isBlank(smtpHost) ) {
			LOGGER.warn("Mail server not configured - insert mail.smtp.host attribute in lconfig.xml");
			return;
		}
		
		Properties props = new Properties();
		Boolean needAuth = !StringUtils.isBlank(smtpUser) && !StringUtils.isBlank(smtpPassword);
		props.setProperty("mail.smtps.auth", String.valueOf(needAuth));
		props.setProperty("mail.smtps.host", smtpHost);
		props.setProperty("mail.smtps.port", String.valueOf(smtpPort));
		props.setProperty("mail.user", smtpUser);
		props.setProperty("mail.password", smtpPassword);
		
		session = Session.getDefaultInstance(props);
		
		internetAddress = new InternetAddress(fromAddress, fromPerson, "utf-8");
	}

	public void sendMail(String address, String subject, String body) {
		if (session == null) {
			LOGGER.info("Send [" + subject + "]=" + body);
			return;
		}

		if (StringUtils.isEmpty(address)) {
			return;
		}

		try {
			MimeMessage message = new MimeMessage(session);
			message.setSubject(subject, "UTF-8");
			message.setFrom(internetAddress);

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/plain; charset=UTF-8");
			multipart.addBodyPart(messageBodyPart);
			// Associate multi-part with message
			message.setContent(multipart);
			message.saveChanges();
			
			Transport trnsport;
		    trnsport = session.getTransport("smtps");
		    trnsport.connect(smtpUser, smtpPassword);
		    trnsport.sendMessage(message, message.getAllRecipients());
		    trnsport.close();
		} catch (MessagingException ex) {
			LOGGER.error("Error delivering message", ex);
		} catch (Throwable th) {
			th.printStackTrace();
			LOGGER.error("Error sending message", th);
		}
	}

	public void sendHTML(String address, String subject, String body) {
		if (session == null) {
			LOGGER.info("Send [" + subject + "]=" + body);
			return;
		}

		if (StringUtils.isEmpty(address)) {
			return;
		}

		try {
			MimeMessage message = new MimeMessage(session);
			message.setSubject(subject, "UTF-8");
			
			//fromAddress = new String(fromAddress.getBytes(), Charset.forName("utf-8"));
			message.setFrom(internetAddress);

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html; charset=UTF-8");
			multipart.addBodyPart(messageBodyPart);

			// Associate multi-part with message
			message.setContent(multipart);
			message.saveChanges();

			Transport trnsport;
		    trnsport = session.getTransport("smtps");
		    trnsport.connect(smtpUser, smtpPassword);
		    trnsport.sendMessage(message, message.getAllRecipients());
		    trnsport.close();
		} catch (MessagingException ex) {
			LOGGER.error("Error delivering message", ex);
		} catch (Throwable th) {
			th.printStackTrace();
			LOGGER.error("Error sending message", th);
		}
	}

	private MimeMultipart createMultipart(String type) {
		String subType;
		try {
			subType = new MimeType("multipart/" + type).getSubType();
		} catch (MimeTypeParseException me) {
			subType = type;
		}
		return new MimeMultipart(subType);
	}

	public void sendHTML(String address, String subject, String body, byte[] attachment, String attachmentName) {
		ByteArrayDataSource dataSource = new ByteArrayDataSource(attachment, "application/octet-stream");
		sendHTML(address, subject, body, dataSource, attachmentName);
	}
	
	public void sendHTML(String address, String subject, String body, File attachment, String attachmentName) {
		FileDataSource dataSource = new FileDataSource(attachment);
		sendHTML(address, subject, body, dataSource, attachmentName);
	}
	
	private void sendHTML(String address, String subject, String body, DataSource attachment, String attachmentName) {
		if (session == null) {
			LOGGER.info("Send [" + subject + "]=" + body);
			return;
		}

		if (StringUtils.isEmpty(address)) {
			return;
		}

		try {
			MimeMessage message = new MimeMessage(session);
			message.setSubject(subject, "UTF-8");

			//fromAddress = new String(fromAddress.getBytes(), Charset.forName("utf-8"));
			message.setFrom(internetAddress);

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));

			MimeMultipart mixed = createMultipart("mixed");
			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(MimeUtility.fold(0, body), "text/html; charset=UTF-8");
			messageBodyPart.setHeader("Content-Transfer-Encoding", "8bit");
			mixed.addBodyPart(messageBodyPart);

			BodyPart attachmentPart = new MimeBodyPart();

			MimetypesFileTypeMap map = new MimetypesFileTypeMap();
			String contentType = map.getContentType(attachmentName);
			if (contentType == null  ) contentType = "application/octet-stream";

			attachmentPart.setDataHandler(new DataHandler(attachment));
			attachmentPart.addHeader("Content-Type", contentType);

			//Charset charset = Charset.forName("US-ASCII");
			//if ( charset.newEncoder().canEncode(attachmentName)) {
				attachmentName = MimeUtility.encodeText(attachmentName, "UTF-8", "Q");
			//}

			attachmentPart.setFileName(attachmentName);
			attachmentPart.setDisposition(Part.ATTACHMENT);
			mixed.addBodyPart(attachmentPart);

			// Associate multi-part with message
			message.setContent(mixed);
			message.saveChanges();

			Transport trnsport;
		    trnsport = session.getTransport("smtps");
		    trnsport.connect(smtpUser, smtpPassword);
		    trnsport.sendMessage(message, message.getAllRecipients());
		    trnsport.close();
		} catch (MessagingException ex) {
			LOGGER.error("Error delivering message", ex);
		} catch (Throwable th) {
			th.printStackTrace();
			LOGGER.error("Error sending message", th);
		}
	}

	public void sendHTML(String address, String subject, String filename, String[] keys, String[] values) {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
			String body = IOUtils.toString(in, "UTF-8");
			body = fillTemplate(body, keys, values);
			sendHTML(address, subject, body);
		} catch (Throwable th) {
			LOGGER.error("Cannot send mail", th);
		} finally {
			try {
				if (in != null) in.close();
			} catch (Throwable th) {
			}
		}
	}
	
	public void sendHTML(String address, String subject, String body, String[] urlsInline ) {
		if (session == null) {
			LOGGER.info("Send [" + subject + "]=" + body);
			return;
		}

		if (StringUtils.isEmpty(address)) {
			return;
		}

		try {
			MimeMessage message = new MimeMessage(session);
			message.setSubject(subject, "UTF-8");
			message.setFrom(internetAddress);

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html; charset=UTF-8");
			multipart.addBodyPart(messageBodyPart);
			for(String urlString : urlsInline) {
				MimeBodyPart part = new MimeBodyPart();
				URL url = new URL(urlString);
				String name = url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
				part.setFileName(name);
				part.setDisposition(Part.INLINE);
				byte buffer[] = new byte[1024];
				int countReadByte;
				byte[] image = new byte[0];
				InputStream is = url.openStream();
				while( (countReadByte = is.read(buffer)) > 0){
					image = ArrayUtils.addAll(image, ArrayUtils.subarray(buffer, 0, countReadByte));
				}
				part.setContentID(name);
				DataSource ds = new AttachmentDataSource(url.openStream(), name);
				part.setDataHandler(new DataHandler( ds ) );
				part.setContent(image, ds.getContentType());
				multipart.addBodyPart(part);
			}
			
			// Associate multi-part with message
			message.setContent(multipart);
			message.saveChanges();
			
			Transport trnsport;
		    trnsport = session.getTransport("smtps");
		    trnsport.connect(smtpUser, smtpPassword);
		    trnsport.sendMessage(message, message.getAllRecipients());
		    trnsport.close();
		} catch (MessagingException ex) {
			LOGGER.error("Error delivering message", ex);
		} catch (Throwable th) {
			th.printStackTrace();
			LOGGER.error("Error sending message", th);
		}
	}
	
	public void sendMail(String address, String subject, String filename, String[] keys, String[] values) {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
			String body = IOUtils.toString(in, "UTF-8");
			body = fillTemplate(body, keys, values);
			sendMail(address, subject, body);
		} catch (Throwable th) {
			LOGGER.error("Cannot send mail", th);
		} finally {
			try {
				if (in != null) in.close();
			} catch (Throwable th) {
			}
		}
	}
	
	public String fillTemplate(String body, String[] keys, String[] values) {
		for (int i = 0; i < keys.length; i++) {
			int ndx = body.indexOf(keys[i]);
			if (ndx >= 0) {
				body = body.replace(keys[i], values[i]);
				i--;
			}
		}
		return body;
	}
	
	private class AttachmentDataSource implements DataSource {
		private InputStream is;
		private String name;

		private AttachmentDataSource(InputStream is, String name) {
			this.is = is;
			this.name = name;
		}

		public InputStream getInputStream() throws IOException {
			return is;
		}

		public OutputStream getOutputStream() throws IOException {
			// not supposed to call
			throw new IllegalArgumentException();
		}

		public String getContentType() {
			// check if content type is broken
			return "image/gif";
		}

		public String getName() {
			return name;
		}
	}
}
