package ru.carservice.mail;

public interface ISendMailHelper {

	void sendFormMessage(String name, String phone, String email, String message, String sendEmail);
	
}
