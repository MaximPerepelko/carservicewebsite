package ru.carservice.mail;

import java.io.File;

public interface IMailService {
	
	void sendMail(String address, String subject, String body);

	void sendHTML(String address, String subject, String body);

	void sendHTML(String address, String subject, String body, File attachment, String attachmentName);

	void sendHTML(String address, String subject, String filename, String[] keys, String[] values);

	void sendMail(String address, String subject, String filename, String[] keys, String[] values);
	
	void sendHTML(String address, String subject, String body, String[] urlsInline );
}
