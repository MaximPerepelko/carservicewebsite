package ru.carservice.boot;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import ru.carservice.config.DataBaseConfig;
import ru.carservice.config.PropertySourcesConfig;
import ru.carservice.config.SecurityConfig;
import ru.carservice.config.WebConfig;
import ru.carservice.util.ajax.AjaxUpdateFilter;

public class ApplicationInitializer implements WebApplicationInitializer {

    private static final String DISPATCHER_SERVLET_NAME = "dispatcher";
    private static final String DISPATCHER_SERVLET_MAPPING = "/";

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
	    AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
	    rootContext.register(PropertySourcesConfig.class);
	    rootContext.register(DataBaseConfig.class);
	    rootContext.register(WebConfig.class);
	    rootContext.register(SecurityConfig.class);
	
	    ServletRegistration.Dynamic dispatcher = servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(rootContext));
	    dispatcher.setLoadOnStartup(1);
	    dispatcher.setAsyncSupported(true);
	    dispatcher.addMapping(DISPATCHER_SERVLET_MAPPING);
	    
	    FilterRegistration.Dynamic ajax = servletContext.addFilter("ajaxUpdateFilter", new AjaxUpdateFilter());
	    ajax.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "/*");
	    
        FilterRegistration.Dynamic security = servletContext.addFilter(AbstractSecurityWebApplicationInitializer.DEFAULT_FILTER_NAME, DelegatingFilterProxy.class);
        security.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");

	    FilterRegistration.Dynamic encoding = servletContext.addFilter("encodingFilter", new CharacterEncodingFilter());
	    encoding.setInitParameter("encoding", "UTF-8");
	    encoding.setInitParameter("forceEncoding", "true");
	    encoding.addMappingForUrlPatterns(null, true, "/*");
	    encoding.setAsyncSupported(true);
	    
	    servletContext.addListener(new ContextLoaderListener(rootContext));
    }
}
