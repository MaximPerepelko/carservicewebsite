package ru.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.Contacts;

@Repository
public interface ContactsRepository extends JpaRepository<Contacts, Integer>, JpaSpecificationExecutor<Contacts> {
	
}
