package ru.carservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.ContentPageEntity;
import ru.carservice.model.ContentPageEntityImage;

@Repository
public interface ContentPageEntityImageRepository extends JpaRepository<ContentPageEntityImage, Integer>, JpaSpecificationExecutor<ContentPageEntityImage> {
	
	List<ContentPageEntityImage> findByContentPageEntity(ContentPageEntity contentPageEntity);
	
}
