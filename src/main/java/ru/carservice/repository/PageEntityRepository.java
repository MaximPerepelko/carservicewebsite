package ru.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.PageEntity;

@Repository
public interface PageEntityRepository extends JpaRepository<PageEntity, Integer>, JpaSpecificationExecutor<PageEntity> {

	PageEntity findByType(PageEntity.Type type);
	
}
