package ru.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.TireService;

@Repository
public interface TireServiceRepository extends JpaRepository<TireService, Integer>, JpaSpecificationExecutor<TireService> {

}
