package ru.carservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.CarService;
import ru.carservice.model.CarServiceImage;

@Repository
public interface CarServiceImageRepository extends JpaRepository<CarServiceImage, Integer>, JpaSpecificationExecutor<CarServiceImage> {
	
	List<CarServiceImage> findByCarService(CarService carService);
	
}
