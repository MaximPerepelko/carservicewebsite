package ru.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.ProjectSettings;

@Repository
public interface ProjectSettingsRepository extends JpaRepository<ProjectSettings, Integer>, JpaSpecificationExecutor<ProjectSettings> {
	
}
