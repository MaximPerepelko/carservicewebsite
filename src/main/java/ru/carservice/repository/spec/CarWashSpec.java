package ru.carservice.repository.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import ru.carservice.model.CarWash;
import ru.carservice.model.filter.BaseFilter;

public class CarWashSpec {
    public static Specification<CarWash> filter(final BaseFilter filter) {
        return new Specification<CarWash>() {

			public Predicate toPredicate(Root<CarWash> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
              	query.distinct(true);
              	
                if(filter.getVisible() != null) {
                	predicates.add(builder.equal(root.get("visible"), filter.getVisible()));
                }

                if (filter.getPath() != null) {
                	predicates.add(builder.equal(root.get("path"), filter.getPath()));
                }
                
                if(StringUtils.isNotBlank(filter.getQuery())) {
                    predicates.add(builder.like(builder.lower(root.get("name").as(String.class)),
                            "%" + filter.getQuery().toLowerCase() + "%"));
                }

                return builder.and(predicates.toArray(new Predicate[0]));
            }
        };
    }
}
