package ru.carservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.carservice.model.News;
import ru.carservice.model.NewsImage;

@Repository
public interface NewsImageRepository extends JpaRepository<NewsImage, Integer>, JpaSpecificationExecutor<NewsImage> {
	
	List<NewsImage> findByNews(News news);
	
}
