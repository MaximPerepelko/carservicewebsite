package ru.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.ContentPageEntity;

@Repository
public interface ContentPageEntityRepository extends JpaRepository<ContentPageEntity, Integer>, JpaSpecificationExecutor<ContentPageEntity> {

	ContentPageEntity findByType(ContentPageEntity.Type type);
	
}
