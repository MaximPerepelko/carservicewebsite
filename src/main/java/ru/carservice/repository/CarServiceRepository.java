package ru.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.CarService;

@Repository
public interface CarServiceRepository extends JpaRepository<CarService, Integer>, JpaSpecificationExecutor<CarService> {
	
	CarService getByPath(String path);
	
}
