package ru.carservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ru.carservice.model.CarWash;
import ru.carservice.model.CarWashImage;

@Repository
public interface CarWashImageRepository extends JpaRepository<CarWashImage, Integer>, JpaSpecificationExecutor<CarWashImage> {
	
	List<CarWashImage> findByCarWash(CarWash carWash);
	
}
