<%@ tag pageEncoding="utf-8" %>
<%@ include file="../../views/include.jsp" %>

<%@ attribute name="initScript" fragment="true" %>
<%@ attribute name="htmlTitle" type="java.lang.String" %>
<%@ attribute name="htmlDescription" type="java.lang.String" %>
<%@ attribute name="htmlKeywords" type="java.lang.String" %>

<c:if test="${empty htmlDescription}">
	<c:set var="htmlDescription" value="${settings.metaDescription}"/>
</c:if>
<c:if test="${empty htmlKeywords}">
	<c:set var="htmlKeywords" value="${settings.metaKeywords}"/>
</c:if>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>${htmlTitle}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="Maxim Perepelko"/>
    <meta name="description" content="${htmlDescription}"/>
    <meta name="keywords" content="${htmlKeywords}"/>
    <meta name="viewport" content="width=960px">
    <meta name="format-detection" content="telephone=no">
    
    <link rel="icon" href="/resources/favicon.ico" type="/favicon.ico"/>
	<link rel="shortcut icon" href="/resources/favicon.ico" />
	
    <link type="text/css" href="/resources/css/style.css" rel="stylesheet" media="all"/>  

	<script src="/resources/js/libs/jquery-2.0.2.min.js" type="text/javascript"></script>
	<script src="/resources/js/libs/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<script src="/resources/js/libs/jquery.timers.js" type="text/javascript"></script>
	<script src="/resources/js/libs/plugin/camera.js" type="text/javascript"></script>
	<script src="/resources/js/libs/plugin/forms.js" type="text/javascript"></script>
	<script src="/resources/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="/resources/js/libs/jquery.equalheights.js" type="text/javascript"></script>
	<script src="/resources/js/libs/jquery.mobilemenu.js" type="text/javascript"></script>
	<script src="/resources/js/libs/jquery.ui.totop.js" type="text/javascript"></script>
	<script src="/resources/js/libs/script.js" type="text/javascript"></script>
	<script src="/resources/js/libs/superfish.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
	
	<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="/resources/js/libs/jquery.mobile.customized.min.js" type="text/javascript"></script>
	<!--<![endif]-->
	
	<script src="/resources/js/commons.js" type="text/javascript"></script>
	<script src="/resources/js/util.js" type="text/javascript"></script>
    <script src="/resources/js/carservice.js" type="text/javascript"></script>

    <!--[if lt IE 9]>
    	<script src="/resources/js/libs/plugin/html5shiv.js"></script>
    <![endif]-->
</head>
<body class="${isMain ? 'page1' : ''}" id="top">

	<jsp:include page="../../views/web/header.jsp" />
	
	<c:if test="${!isMain}">
		<div class="content">
	  		<div class="container_16">
	</c:if>
	
				<jsp:doBody/>
				
	<c:if test="${!isMain}">
			</div>
		</div>
	</c:if>
	
	<jsp:include page="../../views/web/footer.jsp" />

	<script type="text/javascript">
	    $(document).ready(function() {
	        L.initBase();
	        <jsp:invoke fragment="initScript" />
	    });
	</script>

</body>
</html>