<%@ include file="../../views/include.jsp" %>
<%@ tag pageEncoding='UTF-8' %>

<%@ attribute name="type" required="true" type="java.lang.String" %>

<div class="grid_9 prefix_1">
	<h2>Форма обратной связи</h2>
	
	<form id="form">
		<div class="success_wrapper">
			<div class="success">
				<strong>Спасибо!</strong> <br> Ваше сообщение успешно отправлено.
			</div>
		</div>
		
		<fieldset>
		
			<input type="hidden" name="type" value="${type}">
			
			<label class="name"> 
				<input type="text" value="Имя:"/>
				<br class="clear"/> 
				<span class="error error-empty">*Некорректно заполнено имя.</span>
				<span class="empty error-empty">*Необходимо заполнить поле.</span>
			</label> 
			
			<label class="email"> 
				<input type="text" value="E-mail:"/>
				<br class="clear"/> 
				<span class="error error-empty">*Некорректно заполнен e-mail.</span>
				<span class="empty error-empty">*Необходимо заполнить поле.</span>
			</label> 
			
			<label class="phone"> 
				<input type="tel" value="Телефон:"/>
				<br class="clear"/> 
				<span class="error error-empty">*Некорректно заполнен телефон.</span>
				<span class="empty error-empty">*Необходимо заполнить поле.</span>
			</label> 
			
			<label class="message"> 
				<textarea>Сообщение:</textarea> 
				<br class="clear"/> 
				<span class="error">*Сообщение слишком короткое.</span>
				<span class="empty">*Необходимо заполнить поле.</span>
			</label>			
			<div class="clear"></div>
			
			<div class="btns">
				<a data-type="submit" class="btn">Отправить</a>
				<a data-type="reset" class="btn">Очистить</a>
				<div class="clear"></div>
			</div>
		</fieldset>
	</form>
</div>