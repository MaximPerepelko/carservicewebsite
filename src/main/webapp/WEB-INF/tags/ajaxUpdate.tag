<%@tag description="initComponent" pageEncoding="UTF-8"%>

<%@attribute name="id" required="true" type="java.lang.String" %>

<div id="${id}">
	<jsp:doBody/>
</div>


