<%@ tag import="java.util.List" %>
<%@ tag import="java.util.ArrayList" %>
<%@ include file="../../views/include.jsp" %>

<%@ tag pageEncoding='UTF-8' %>
<%@ attribute name="page" required="true" type="org.springframework.data.domain.Page" %>
<%@ attribute name="url" required="true" type="java.lang.String" %>

<jsp:doBody/>

<%--<c:if test="${page.number != 1}"><a class="curved" href="${url}page=${page.number-1}">Back</a></c:if>--%>

<jsp:useBean id="page" type="org.springframework.data.domain.Page"/>

<%
    int pc = 5;

    int number = page.getNumber() + 1;
    int start = Math.max(1, number - (pc /2));
    int over = start + pc - page.getTotalPages() - 1;
    if ( over > 0 ) {
        start -= over;
    }
    if ( start < 1 ) {
        start = 1;
    }

    List<Integer> pages = new ArrayList<Integer>();
    int max = page.getTotalPages() + 1;
    int c = 0;
    while(start < max && c < pc) {
        pages.add(start++);
        c++;
    }
    jspContext.setAttribute("pages", pages);
%>

<ul class="pagination">
	<c:if test="${fn:length(pages) gt 1}">
	    <c:forEach var="p" items="${pages}">
	        <c:choose>
	            <c:when test="${p eq (page.number+1)}">
	                <li class="active"><span>${p}</span></li>
	            </c:when>
	            <c:otherwise>
	                <li><a href="${url}page=${p}">${p}</a></li>
	            </c:otherwise>
	        </c:choose>
	    </c:forEach>
    </c:if>
</ul>



<%--<c:if test="${page.number != page.totalPages}"><a class="curved" href="${url}page=${page.number+1}">Forward</a></c:if>--%>

