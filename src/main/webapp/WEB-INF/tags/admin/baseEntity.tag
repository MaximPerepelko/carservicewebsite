<%@ include file="../../views/include.jsp" %>
<%@ tag pageEncoding='UTF-8' %>

<%@ attribute name="hideable" required="false" type="java.lang.Boolean" %>

<c:if test="${empty hideable}">
	<c:set var="hideable" value="false"/>
</c:if>

<jsp:doBody/>

<section>
  	<form:hidden path="id"/>
  
	<label class="label">Название <span>*</span></label>
	<label class="input">
		<form:input class="form-control" type="text" path="name"/>
		<form:errors class="help-block error" path="name"/>
	</label>
</section>
	
<c:if test="${hideable}">
	<section>
		<label class="checkbox">
			<form:checkbox path="visible"/>
			<i></i>
			Отображать на сайте
		</label>
	</section>
</c:if>