<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ include file="../include.jsp"%>

<tw:page>
	<jsp:body>
	
		<div class="carservice-page">
			<div class="grid_16">
				
				<h2>${carService.name}</h2>			
				<div class="content-text">${carServiceText}</div>
			</div>
		</div>
			
	</jsp:body>
</tw:page>