<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<!-- HEADER -->
<header> 
	<div class="container_16">
		<div class="grid_16">
			<h1>
				<a href="/"> <img src="/resources/img/web/logo.png" alt="Car Service"></a>
			</h1>
	
			<div class="menu_block">
				<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul class="sf-menu">
						<!--  <li class="current "><a href="index.html">Home <strong>who we are</strong></a>
							<ul>
								<li><a href="#">history</a></li>
								<li><a href="#">offers</a></li>
								<li><a href="#">news</a>
									<ul>
										<li><a href="#">fresh</a></li>
										<li><a href="#">archive</a></li>
									</ul>								
								</li>
							</ul>						
						</li>-->
						
						<li><a href="/about/">О нас <strong>о нас</strong></a></li>
						<li><a href="/carwash/">Мойка <strong>мойка машин</strong></a></li>
						<li><a href="/carservice/">Техцентр <strong>техцентр</strong></a></li>
						<li><a href="/aerography/">Аэрография <strong>аэрография</strong></a></li>
						<li><a href="/contacts/">Контакты <strong>контакты</strong></a></li>
					</ul>
				</nav>
	
				<div class="clear"></div>
			</div>
		</div>
	</div>
</header>
<!-- HEADER -->
