<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<!-- FOOTER -->
<footer>
	<div class="f_top">
		<div class="container_16">
			<div class="grid_16">
				<div class="bd"></div>
			</div>
			<div class="grid_5 maxheight1">
				<span>e-mail: <a href="#">info@demolink.org</a></span>
			</div>
			<div class="grid_6 maxheight1 ver">
				<div class="socials">
					<a href="#"></a> <a href="#"></a> <a href="#"></a>
				</div>
			</div>
			<div class="grid_5 maxheight1">
				<address>
					28 Jackson Blvd Ste 1020 Chicago, <br> IL 60604-2340
				</address>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	
	<div class="container_16">
		<div class="grid_16">
			<div class="copy">
				<strong>IQcar </strong> &copy; 2013 &bull; 
				<a href="index-5.html">Privacy Policy</a> 
				More 
				<a rel="nofollow" href="http://www.templatemonster.com/category/car-repair-website-templates/" target="_blank">Car Repair Website Templates at TemplateMonster.com</a>
			</div>
		</div>
	</div>
</footer>
<!-- FOOTER -->