<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ include file="../include.jsp"%>

<tw:page>
	<jsp:body>
	
	<div class="carwash-list-page">
		<div class="grid_5">
			<c:if test="${not empty carWashes}">
				<h2>Прайс лист</h2>
	        	        
				<ul class="list l1">
					<c:forEach var="item" items="${carWashes}">
						<li><a href="#">${item.carWash.name}</a></li>
					</c:forEach>
		      	</ul>
	      	</c:if>
	    </div>
		
		<div class="grid_10">
			<h2>${pageEntity.name}</h2>
	      
	      	<c:if test="${not empty carWashes}">
		      	<c:forEach var="item" items="${carWashes}">
		      		<div class="mtn">
	        			<img src="${item.image}" alt="${item.carWash.name}" class="img_inner fleft">
	        			
	        			<div class="extra_wrapper">
            				<div class="text2">
								<a href="/carwash/${item.carWash.path}/">${item.carWash.name}</a>
							</div>
	          				<p>${item.carWash.shortText}</p>
	          				<a href="/carwash/${item.carWash.path}/" class="btn">читать далее</a>
	        			</div>
	      			</div>
		      	</c:forEach>
	      	</c:if>
	    </div>
	</div>
	
	</jsp:body>
</tw:page>