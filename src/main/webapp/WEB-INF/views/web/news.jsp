<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ include file="../include.jsp"%>

<tw:page>
	<jsp:body>
	
		<div class="news-page">
			<div class="grid_16">
				
				<h2>${news.name}</h2>	
				<div class="text2">${dateUtils:formatFull(news.date)}</div>		
				<div class="content-text">${newsText}</div>
			</div>
		</div>
			
	</jsp:body>
</tw:page>