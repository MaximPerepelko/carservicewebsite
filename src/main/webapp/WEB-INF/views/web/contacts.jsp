<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ include file="../include.jsp"%>

<tw:page>
	<jsp:attribute name="initScript">
		L.initContactsPage();
	</jsp:attribute>
	<jsp:body>
	
		<div class="contacts-page">
			<div class="grid_5">
      			<h2>Контакты</h2>
      			
     			<address>
                	<div>${contacts.address}</div>
                    <div>
                    	<span>Телефон: <a href="tel:${contacts.phone}">${contacts.phone}</a></span><br/>
                    	<span>Телефон мойки: <a href="tel:${contacts.phoneCarWash}">${contacts.phoneCarWash}</a></span>
                    </div>
                    <div>
                    	<span>E-mail: <a href="mailto:${contacts.email}">${contacts.email}</a></span><br/>
                    	<span>E-mail мойки: <a href="mailto:${contacts.emailCarWash}">${contacts.emailCarWash}</a></span><br/>
                    	<span>E-mail сервиса: <a href="mailto:${contacts.emailCarService}">${contacts.emailCarService}</a></span><br/>
                    	<span>E-mail мастерам: <a href="mailto:${contacts.emailMaster}">${contacts.emailMaster}</a></span>
                    </div>
				</address>
						
      			<c:if test="${not empty contacts.address}">
            		<div class="map">
						<figure class="">                        	
							<div id="map" address="${contacts.address}"></div>						
               			</figure>
					</div>
				</c:if>
			</div>
			
			<tw:form type="info"/>
		</div>
	</jsp:body>
</tw:page>