<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ include file="../include.jsp"%>

<tw:page>
	<jsp:body>
	
		<div class="content-page">
			<div class="grid_16">
				
				<h2>${contentPageEntity.name}</h2>			
				<div class="content-text">${contentPageText}</div>
			</div>
		</div>
		
		<tw:form type="info"/>
		
	</jsp:body>
</tw:page>