<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ include file="../include.jsp"%>

<tw:page>
	<jsp:body>
	
	<div class="carservice-list-page">
		<div class="grid_5">		
			<c:if test="${not empty carServices}">
				<h2>Прайс лист</h2>
				
				<ul class="list l1">
					<c:forEach var="item" items="${carServices}">
						<li><a href="#">${item.name}</a></li>
					</c:forEach>
		      	</ul>
	      	</c:if>
	      	
	      	<c:if test="${not empty tireServices}">
	      		<h2 class="head1">Шиномонтаж</h2>
	      	    		
     			<ul class="list l2">
     				<c:forEach var="item" items="${tireServices}">
						<li><a href="#">${item.name}</a></li>
					</c:forEach>
     			</ul>
     		</c:if>
	    </div>
		
		<div class="grid_10">
			<h2>${pageEntity.name}</h2>
	      
	      	<c:if test="${not empty carServices}">
		      	<c:forEach var="item" items="${carServices}">
		      		<div class="mtn">
		      			<img src="${item.image}" alt="${item.carService.name}" class="img_inner fleft">
		      			
						<div class="extra_wrapper">
							<div class="text2">
								<a href="/carservice/${item.carService.path}/">${item.carService.name}</a>
							</div>
          					<p>${item.carService.shortText}</p>
          					<a href="/carservice/${item.carService.path}/" class="btn">читать далее</a>
        				</div>
       				</div>
		      	</c:forEach>
	      	</c:if>
	    </div>
	</div>
	
	</jsp:body>
</tw:page>