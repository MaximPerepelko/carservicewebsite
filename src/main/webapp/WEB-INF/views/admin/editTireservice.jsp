<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<ta:page title="Страница услуги шиномонтажа">
    <jsp:body>

        <div class="tirewash-editor">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-pencil-square-o fa-fw fa"></i>${tireService.id > 0 ? tireService.name : 'Новая услуга'}
                        <a href="/admin/${activeMenu.href}" class="btn btn-default right-header-button" role="button">Список</a>
                        <a href="/admin/${activeMenu.href}edit/" class="btn btn-info right-header-button margin-right-5" role="button">Добавить</a>
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <div class="jarviswidget jarviswidget-color-blueDark">
                        <header role="heading">
                            <h2>${tireService.id > 0 ? tireService.name : 'Новая услуга'}</h2>
                        </header>
                        
                        <div role="content">
                            <div class="widget-body no-padding">
                                <form:form class="smart-form" commandName="tireService" role="form" method="post">
                                    <fieldset>
                                       	 <ta:baseEntity hideable="true"/>
                                    </fieldset>
                                       									
                                    <footer>
                                        <button class="btn btn-primary" type="submit" name="save">
                                            <i class="fa fa-save"></i> Сохранить
                                        </button>
                                    </footer>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </jsp:body>
</ta:page>