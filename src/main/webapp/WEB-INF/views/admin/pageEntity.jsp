<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<ta:page title="Страница ${pageEntity.name}">
    <jsp:body>

        <div class="page-entity">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-pencil-square-o fa-fw fa"></i>Страница ${pageEntity.name}
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <div class="jarviswidget jarviswidget-color-blueDark">
                        <header role="heading">
                            <h2>${pageEntity.name}</h2>
                        </header>
                        
                        <div role="content">
                            <div class="widget-body no-padding">
                                <form:form class="smart-form" commandName="pageEntity" role="form" method="post">
                                    <fieldset>
                                       	<ta:baseEntity/>
									</fieldset>
									
									<fieldset>									
                              			<ta:baseEntitySeo/>
                                    </fieldset>
                                    
                                    <footer>
                                        <button class="btn btn-primary" type="submit" name="save">
                                            <i class="fa fa-save"></i> Сохранить
                                        </button>
                                    </footer>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </jsp:body>
</ta:page>