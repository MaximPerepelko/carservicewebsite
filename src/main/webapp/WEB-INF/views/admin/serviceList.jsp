<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<ta:page title="Список услуг">
	<jsp:attribute name="initScript">
		E.initList();
	</jsp:attribute>
    <jsp:body>
        <div class="list-area sys-list">
			<ta:list list="${page.content}"/>
        </div>
    </jsp:body>
</ta:page>

