<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<ta:page title="Контакты">
    <jsp:body>

        <div class="page-entity">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-pencil-square-o fa-fw fa"></i>Контакты
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <div class="jarviswidget jarviswidget-color-blueDark">
                        <header role="heading">
                            <h2>Контакты</h2>
                        </header>
                        
                        <div role="content">
                            <div class="widget-body no-padding">
                                <form:form class="smart-form" commandName="contacts" role="form" method="post">
                                	<fieldset>
                                		<form:hidden path="id"/>
                                	
	                                    <section>
										  	<label class="label">Адрес <span>*</span</label>
											<label class="input">
												<form:input class="form-control" type="text" path="address"/>
												<form:errors class="help-block error" path="address"/>
											</label>
										</section>
										
										<section>
										  	<label class="label">Телефон <span>*</span</label>
											<label class="input">
												<form:input class="form-control" type="text" path="phone"/>
												<form:errors class="help-block error" path="phone"/>
											</label>
										</section>
										<section>
										  	<label class="label">Телефон мойки</label>
											<label class="input">
												<form:input class="form-control" type="text" path="phoneCarWash"/>
											</label>
										</section>
										
										<section>
										  	<label class="label">E-mail <span>*</span</label>
											<label class="input">
												<form:input class="form-control" type="text" path="email"/>
												<form:errors class="help-block error" path="email"/>
											</label>
										</section>
										<section>
										  	<label class="label">E-mail мойки</label>
											<label class="input">
												<form:input class="form-control" type="text" path="emailCarWash"/>
											</label>
										</section>
										<section>
										  	<label class="label">E-mail сервиса</label>
											<label class="input">
												<form:input class="form-control" type="text" path="emailCarService"/>
											</label>
										</section>
										<section>
										  	<label class="label">E-mail мастерам</label>
											<label class="input">
												<form:input class="form-control" type="text" path="emailMaster"/>
											</label>
										</section>										
									</fieldset>
									
									<fieldset>									
                              			<ta:baseEntitySeo/>
                                    </fieldset>
                                    
                                    <footer>
                                        <button class="btn btn-primary" type="submit" name="save">
                                            <i class="fa fa-save"></i> Сохранить
                                        </button>
                                    </footer>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </jsp:body>
</ta:page>