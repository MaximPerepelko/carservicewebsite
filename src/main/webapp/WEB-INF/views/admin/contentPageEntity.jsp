<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<ta:page title="Страница ${contentPageEntity.name}">
	<jsp:attribute name="initScript">
		E.initContentPageEntity();
	</jsp:attribute>
    <jsp:body>

        <div class="content-page-entity">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-pencil-square-o fa-fw fa"></i>Страница ${contentPageEntity.name}
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <div class="jarviswidget jarviswidget-color-blueDark">
                        <header role="heading">
                            <h2>${contentPageEntity.name}</h2>
                        </header>
                        
                        <div role="content">
                            <div class="widget-body no-padding">
                                <form:form class="smart-form" commandName="contentPageEntity" role="form" method="post">
                                    <fieldset>
                                       	<ta:baseEntity/>									
									
										<section>
											<label class="label">Описание</label>
	                                        <label class="input">
	                                        	<form:textarea class="form-control wysiwyg" path="text" />
	                                        </label>
	                                    </section>	
									</fieldset>
									
									<fieldset>									
                              			<ta:baseEntitySeo/>
                                    </fieldset>
                                    
                                    <footer>
                                        <button class="btn btn-primary" type="submit" name="save">
                                            <i class="fa fa-save"></i> Сохранить
                                        </button>
                                    </footer>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
			<c:if test="${contentPageEntity.id != 0}">
				<div class="row image-panel content-image">
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="jarviswidget jarviswidget-color-blueDark">
							<header role="heading">
								<h2>Изображения для текста</h2>
							</header>
							<div role="content">
								<div class="widget-body">
									<div class="superbox list-images">
										<t:ajaxUpdate id="image-list">
											<c:forEach items="${images}" var="image">
												<div class="superbox-list block-image" iid="${image.id}">
													<img src="${image.image}" class="superbox-img" data-img="${image.image}" width="150"/>
													<div class="code">#[image=${image.id}]</div>
												</div>
											</c:forEach>
										</t:ajaxUpdate>
										<div class="superbox-float"></div>
									</div>
										
									<jsp:include page="panel/upload_image_panel.jsp">
										<jsp:param name="type" value="${contentPageEntity.type}"/>
									    <jsp:param name="entityId" value="${contentPageEntity.id}"/>
									    <jsp:param name="crop" value="false"/>
									    <jsp:param name="isMultiple" value="true"/>
									</jsp:include>
								</div>
							</div>
						</div>
					</div>				
				</div>			
			</c:if>
        </div>
    </jsp:body>
</ta:page>