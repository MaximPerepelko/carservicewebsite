<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp" %>

<ta:page title="Страница новости">
	<jsp:attribute name="initScript">
		E.initNewsPage();
	</jsp:attribute>
    <jsp:body>

        <div class="news-editor">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-pencil-square-o fa-fw fa"></i>${news.id > 0 ? news.name : 'Новая новость'}
                        <a href="/admin/${activeMenu.href}" class="btn btn-default right-header-button" role="button">Список</a>
                        <a href="/admin/${activeMenu.href}edit/" class="btn btn-info right-header-button margin-right-5" role="button">Добавить</a>
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10">
                    <div class="jarviswidget jarviswidget-color-blueDark">
                        <header role="heading">
                            <h2>${news.id > 0 ? news.name : 'Новая новость'}</h2>
                        </header>
                        
                        <div role="content">
                            <div class="widget-body no-padding">
                                <form:form class="smart-form" commandName="news" role="form" method="post">
                                    <fieldset>
                                       	 <ta:baseEntity hideable="true"/>
                                       	 
                                       	 <section>
											<label class="label">Дата <span>*</span></label>
                                            <label class="input">
                                                <form:input class="form-control datepicker" path="date" />
                                                <form:errors class="help-block error" path="date"/>
                                            </label>
                                         </section>	
                                       	 <section>
											<label class="label">Краткое описание</label>
                                            <label class="input">
                                                <form:textarea class="form-control" path="shortText" />
                                            </label>
                                         </section>			
                                       	 <section>
											<label class="label">Описание</label>
                                            <label class="input">
                                                <form:textarea class="form-control wysiwyg" path="text" />
                                            </label>
                                         </section>	
                                      </fieldset>
                                         
									<fieldset>									
                              			<ta:baseEntitySeo showPath="true"/>
                                    </fieldset>
                                       									
                                    <footer>
                                        <button class="btn btn-primary" type="submit" name="save">
                                            <i class="fa fa-save"></i> Сохранить
                                        </button>
                                    </footer>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <c:if test="${news.id != 0}">	
                <div class="row image-panel news-image">
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="jarviswidget jarviswidget-color-blueDark">
							<header role="heading">
								<h2>Изображение превью</h2>
							</header>
							
							<div role="content">
								<div class="widget-body no-padding">
									<t:ajaxUpdate id="image">
										<jsp:include page="panel/upload_image_panel.jsp">
									        <jsp:param name="type" value="NEWS_PREVIEW"/>
									        <jsp:param name="entityId" value="${news.id}"/>
									        <jsp:param name="image" value="${image}"/>
									        <jsp:param name="isMultiple" value="false"/>
									    </jsp:include>
									</t:ajaxUpdate>
								</div>
							</div>
						</div>
					</div>
				</div>	
							
				<div class="row image-panel content-image">
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="jarviswidget jarviswidget-color-blueDark">
							<header role="heading">
								<h2>Изображения для текста</h2>
							</header>
							<div role="content">
								<div class="widget-body">
									<div class="superbox list-images">
										<t:ajaxUpdate id="image-list">
											<c:forEach items="${images}" var="image">
												<div class="superbox-list block-image" iid="${image.id}">
													<img src="${image.image}" class="superbox-img" data-img="${image.image}" width="150"/>
													<div class="code">#[image=${image.id}]</div>
												</div>
											</c:forEach>
										</t:ajaxUpdate>
										<div class="superbox-float"></div>
									</div>
										
									<jsp:include page="panel/upload_image_panel.jsp">
										<jsp:param name="type" value="NEWS_CONTENT"/>
									    <jsp:param name="entityId" value="${news.id}"/>
									    <jsp:param name="crop" value="false"/>
									    <jsp:param name="isMultiple" value="true"/>
									</jsp:include>
								</div>
							</div>
						</div>
					</div>
				</div>			
            </c:if>
        </div>
    </jsp:body>
</ta:page>