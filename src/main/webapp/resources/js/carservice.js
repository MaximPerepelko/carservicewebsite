var L = {

    initBase: function() {      
    	//L.initMap("#map");
    	$(document).ready(function() {
    		$().UItoTop({ 
    			easingType: 'easeOutQuart' 
    		});
    	});	
    },
    
    
    initMainPage: function() {
		$(document).ready(function() {
			jQuery('#camera_wrap').camera({
				loader : false,
				pagination : false,
				thumbnails : false,
				height : '48.95238095238095%',
				caption : false,
				navigation : true,
				fx : 'mosaic'
			});						
		});
    },
    
    initContactsPage: function() {
    	L.initMap("#map");
    },
    
    initMap: function(mapElement) {
	    if ($(mapElement).length) {
	    	var loadMap = function() {
	    		var address = $(mapElement).attr("address");
	    		
	    		var geocoder = new  google.maps.Geocoder();
    		    geocoder.geocode({'address': address}, function(results, status) {
    		    	if (status == google.maps.GeocoderStatus.OK) {
    		    		var markerPosition = results[0].geometry.location;
    		    		var center = results[0].geometry.location;
    		    		
    		            var myOptions = {
			                zoom: 17,
			                scrollwheel: false,
			                center: center,
			                navigationControl: true,
			                navigationControlOptions: {
			                    style: google.maps.NavigationControlStyle.ZOOM_PAN
			                },

			                mapTypeControl: true,
			                mapTypeControlOptions: {
			                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
			                },
			                streetViewControl: true,
			                mapTypeId: google.maps.MapTypeId.ROADMAP
			            };
    		            
    		            var map = new google.maps.Map(document.getElementById(mapElement.split('#')[1]), myOptions);
    		            
    		            var marker = new google.maps.Marker({
    		                position: markerPosition,
    		                map: map,
//    		                icon: "/resources/img/marker.png",
    		                animation: google.maps.Animation.DROP,
//    		                title: $(mapElement).attr('data-description')
    		            });
    		            
    		            google.maps.event.addListener(marker, 'click', function() {
    		                infowindow.open(map, marker);
    		            });
    		    	}  		    
    		    });
	        }   	    
	    	loadMap();
	    	
	        $(window).resize(function() {
	        	loadMap();
	        });
	    }
    }
    
//    
// initMenuPage: function() {
// $(".menu-page").on("click", ".sys-category", function() {
//    		Util.postOnAjax({action: "update", id: $(this).attr("cid")});
//    		
//    		return false;
//    	});
//    	
//    	var initCalculator = function() {
//        	var getCalcItem = function(name, price) {
//        		return "<li class=\"m-c-item\">" +
//        					"<div class=\"m-c-line\">" +
//        						"<div class=\"m-c-n\">" + name + "</div>" +
//        						"<div class=\"sys-c-price m-c-p\">" + price + 
//        							" €<a href=\"#\" class=\"sys-c-remove\"></a>" +
//        						"</div>" +
//        					"</div>" +
//        				"</li>";
//        	};
//    		
//    		$(".menu-page").on("click", ".sys-add-calc", function() {
//        		var item = $(this).parents("li");
//        		var name = item.find(".sys-d-name").text();
//        		var price = parseFloat(item.find(".sys-d-price").text());
//        		var sum = parseFloat($(".menu-page").find(".sys-c-sum").text());
//        		sum += price;
//        		
//        		if (name && !isNaN(price) && !isNaN(sum)) {
//        			$(getCalcItem(name, price)).appendTo(".m-calculator .m-c-body");
//        			$(".m-calculator .sys-c-sum").text(sum);
//        		}
//
//        		return false;
//        	});
//    		
//    		$(".menu-page").on("click", ".sys-c-remove", function() {
//    			var item = $(this).parents("li");
//    			var price = parseFloat(item.find(".sys-c-price").text());
//    			var sum = parseFloat($(".menu-page").find(".sys-c-sum").text());
//    			sum -= price;
//    			
//    			if (!isNaN(price) && !isNaN(sum)) {
//    				item.remove();
//        			$(".m-calculator .sys-c-sum").text(sum);
//        		}
//    			
//    			return false;
//    		});
//    		
//    		$(window).scroll(function() {
//    			var calc = $(".m-calculator");
//    			var scrollTop = $(window).scrollTop();       		
//        		
//        		if (scrollTop > 630) {       			
//        			var top = $(window).scrollTop() - 630;
//        			var menuHeight = $(".c-block.m-left").height();
//        			
//        			if (menuHeight > 0) {
//            			var maxTop = menuHeight - calc.height() - 32;
//            			
//            			if (top > maxTop) {
//            				top = maxTop;
//            			}
//            			calc.css("top", top);
//            			calc.css("position", "absolute");
//        			}
//        		} else {
//        			calc.css("position", "initial");
//        		}
//        	});
//    	}
//    	initCalculator();
//    	
//    	L.initFancyBox();
//    },
//    
//    initPosterPage: function() {
//    	L.initMainImageSliderPanel();  	
//    	L.initFancyBox();    	
//    	C.initLoadable();
//    },
//    
//    initCommentsPage: function() {	
//    	C.initLoadable();
//    	
//    	$(document).on("click", ".sys-add-comment", function() {
//    		var textarea = $(".sys-add").find("textarea[name=comment]");
//    		var text = textarea.val();
//    		if (text == "") {
//    			textarea.addClass("error");
//    		} else {
//    			textarea.removeClass("error");
//        		textarea.val('');        		
//        		Util.postUrl("add-comment", {text: text});
//    		}
//    	});
//    },
//    
//    initMainImageSliderPanel: function() {
//    	var mainImageSliderPanel = $(".mp-main-image-slider");
//    	if(mainImageSliderPanel.length) {
//	    	mainImageSliderPanel.slick({
//	    	  arrows: jQuery.browser.mobile ? false : true,
//	    	  autoplay : true,
//	    	  autoplaySpeed: 3000,
//			  infinite: true,
//			  speed: 300,
//			  slidesToShow: 1,
//			  centerMode: true,
//			  variableWidth: true
//			});
//    	}
//    },
//
//    initNewsList: function() {
//    	C.initLoadable();
//    },
//    
//    initGalleryPage: function() {
//    	var area = $(".gallery-page");
//    	
//    	var initGallery = function() {
//        	var showPhoto = function(albumId) {
//    			var openPopup = function(data) {
//    				var template = area.find(".sys-photo-popup").clone();
//    				var largeSlider = template.find(".large-photo-slider");
//    				var largeContainer = template.find(".large-photo-container");
//    				var previewSlider = template.find(".preview-photo-slider");
//    				var previewContainer = template.find(".preview-photo-container");
//    				
//    				$.each(data, function(index, item) {
//    					if (index <= 0) {
//        					var imageLarge = $("<img/>").attr("src", item.imageLarge);
//        					largeContainer.append($("<div class='ts-image'/>").attr("pid", item.id).append(imageLarge));
//    					}	
//    					if (index <= 9) {
//	    					if (data.length > 1) {
//	        					var imageSmall = $("<img/>").attr("src", item.imageSmall);
//	        					previewContainer.append($("<a class='ts-preview " + (index == 0 ? "active" : "") + "' href='#'/>").attr("pid", item.id).append(imageSmall));
//	    					}
//    					} else {
//    						return false;
//    					}
//    				});
//    				
//    				var initSliders = function() {
//    					var largeSettings = {
//    						container: ".large-photo-container",
//    						scrollPrev: ".scoll-left",
//    						scrollNext: ".scoll-right",
//    						element: ".ts-image",
//    						countScroll: 1,
//    						round: true
//    					};
//    					var scollTo = L.initScrollPanel(largeSlider, largeSettings);
//    					
//    					var previewSettings = {
//    						animateCount: 10,
//    						container: ".preview-photo-container",
//    						scrollPrev: ".scoll-left",
//    						scrollNext: ".scoll-right",
//    						element: ".ts-preview",
//    						countScroll: 1,
//    						round: true
//    					};
//    					L.initScrollPanel(previewSlider, previewSettings);
//    					
//    					previewSlider.find(".ts-preview").on("click", function() {
//    						var pid = $(this).attr("pid");
//    						scollTo(".ts-image[pid=" + pid + "]");
//    						$(".ts-preview").removeClass("active");
//    						$(this).addClass("active");
//    						
//    						return false;
//    					});
//    				};
//    				
//    				var calcSizeTemplate = function() {
//    					var windowWidth = $(window).width();
//    					var windowHeight = $(window).height();
//    					var height = windowHeight - 0.1 * windowHeight;
//    					var width = Math.min(height * 16/9, windowWidth - 0.1 * windowWidth);
//    					template.css({width: width + "px"});
//    				};
//    				var calcHeights = function() {
//    					largeContainer.parents(".ts-photo-popup").height($(window).height() - 20);
//    					var height = previewContainer.height();    					
//    					var heightContainer = largeContainer.parents(".ts-photo-popup").height();    					
//    					template.find(".large-photo-slider").height(heightContainer - height);
//    					
//    					largeContainer.find(".ts-image").each(function(index, element) {
//    						var margin = ($(element).height() - $(element).find("img").height()) / 2;
//    						$(element).css("margin-top", margin);
//    					});
//    				};
//    				$(window).off(".photo-popup").on("resize.photo-popup", function() {
//    				     largeSlider.height("auto");
//    				     largeSlider.find(".ts-image, .scoll-left, .scoll-right").css("opacity", "0");
//    				     
//    				     calcSizeTemplate();
//    				     $.fancybox.update(popup);
//    				     
//    				     largeSlider.stopTime("resize").oneTime(100, function() {
//    				    	 largeSlider.find(".ts-image, .scoll-left, .scoll-right").css("opacity", "1");
//    				     }); 				     
//    				});
//    				calcSizeTemplate();
//    				
//    				var isInited = false;
//    				
//    				var settings = {
//    					closeBtn: true,
//    					padding: 0,
//    					margin: 10,
//    					scrolling: "no",
//    					autoResize: false,
//    					beforeShow: function() {
//    						$(".fancybox-overlay").css({opacity: 0.0});
//    					},
//    					afterShow: function() {
//    						$("html").addClass("gallery");
//    						initSliders();
//    						$(".fancybox-overlay").animate({opacity: 1.0}, 400);   						
//    					},
//    					afterClose: function() {
//    						$("html").removeClass("gallery");
//    					},
//    					onUpdate: function() {
//    						calcHeights();
//    						
//    						if (!isInited) {
//        						$.each(data, function(index, item) {
//        	    					if (index > 0) {
//        	        					var imageLarge = $("<img/>").attr("src", item.imageLarge);
//        	        					largeContainer.append($("<div class='ts-image'/>").attr("pid", item.id).append(imageLarge));
//        	    					}	
//        	    					if (index > 9) {
//        		    					if (data.length > 1) {
//        		        					var imageSmall = $("<img/>").attr("src", item.imageSmall);
//        		        					previewContainer.append($("<a class='ts-preview' href='#'/>").attr("pid", item.id).append(imageSmall));
//        		    					}
//        	    					}
//        	    				});	
//        						$(largeContainer).load(initSliders());
//    						}
//    					}
//    				};
//    				
//    				var popup = $.fancybox(template, settings);
//    			};
//    			
//    			Util._postBaseUrl(albumId + "/get-photo/", {}, function(data) {
//    				if(data.length) {
//    					openPopup(data);
//    				}
//    			});
//    		};
//    		
//        	area.on("click", ".sys-gallery", function() {
//        		var albumId = $(this).parents(".sys-item").attr("aid");
//        		showPhoto(albumId);
//
//        		return false;
//        	});
//        	
//        	area.on("click", ".sys-video", function() {
//        		var item = $(this).parents(".sys-item");
//        		var iFrame = item.find("iframe");
//        		var src = iFrame.attr("src");
//        		
//        		iFrame.attr("src", src + "&autoplay=1");
//        		iFrame.fadeIn();
//        		item.find(".sys-hide").fadeOut();  
//        		
//        		return false;
//        	});
//    	};
//    	initGallery();
//    	
//    	C.initLoadable();
//    },
//    
//    initMap: function(mapElement) {
//	    if ($(mapElement).length) {
//	    	var loadMap = function() {
//	    		var address = $(mapElement).attr("address");
//	    		
//	    		var geocoder = new  google.maps.Geocoder();
//    		    geocoder.geocode({'address': address}, function(results, status) {
//    		    	if (status == google.maps.GeocoderStatus.OK) {
//    		    		var markerPosition = results[0].geometry.location;
//    		    		var center = results[0].geometry.location;
//    		    		
//    		            var myOptions = {
//			                zoom: 17,
//			                scrollwheel: false,
//			                center: center,
//			                navigationControl: true,
//			                navigationControlOptions: {
//			                    style: google.maps.NavigationControlStyle.ZOOM_PAN
//			                },
//
//			                styles: [{stylers: 
//	    						[{ saturation: -100 }]
//	    					}],
//			                
//			                mapTypeControl: true,
//			                mapTypeControlOptions: {
//			                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
//			                },
//			                streetViewControl: true,
//			                mapTypeId: google.maps.MapTypeId.ROADMAP
//			            };
//    		             		          		            
//    		            var map = new google.maps.Map(document.getElementById(mapElement.split('#')[1]), myOptions);
//    		            
//    		            var directionsService = new google.maps.DirectionsService();
//    		            var directionsDisplay = new google.maps.DirectionsRenderer({map: map});
//    		                		 
//    		            var directionInfo = document.getElementById('direction-info');
//    		            map.controls[google.maps.ControlPosition.LEFT_TOP].push(directionInfo);
//    		            
//    		            var input = document.getElementById('map-input');    		           
//    		            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);  		          
//    		            
//    		            var searchBox = new google.maps.places.SearchBox(input);
//    		            searchBox.addListener('places_changed', function() {
//    		                var places = searchBox.getPlaces();
//    		                
//    		                if (!places || places.length == 0) {
//    		                    return;
//    		                }
//    		                routeDirection(results[0].geometry.location, places[0].geometry.location);	
//    		            });
//    		            
//    		            var button = document.getElementById('map-direction');    		           
//    		            map.controls[google.maps.ControlPosition.TOP_LEFT].push(button);
//    		            $(button).click(function() {
//    		            	var places = searchBox.getPlaces();
//    		            	
//    		            	if (!places || places.length == 0) {
//    		            		 geocoder.geocode({'address': $(input).val()}, function(res, status) {
//    		         		    	if (status == google.maps.GeocoderStatus.OK) {
//    		         		    		routeDirection(results[0].geometry.location, res[0].geometry.location);
//    		         		    	 }
//    		            		 });
//     		                } else {
//     		                	routeDirection(results[0].geometry.location, places[0].geometry.location);
//     		                }     		                
//    		            });
//    		            
//    		            function routeDirection(origin, destination) {
//        		            directionsService.route({
//	    		                origin: origin,
//	    		                destination: destination,	    	
//	    		                travelMode: google.maps.TravelMode.DRIVING,
//	    		                avoidTolls: true
//	    		            }, function(response, status) {
//	    		                if (status === google.maps.DirectionsStatus.OK) {
//	    		                	directionsDisplay.setDirections(response);
//	    		                	var point = response.routes[0].legs[0];	    		               
//	    		                    $(directionInfo).find(".distance span").text(point.distance.text);
//	    		                    $(directionInfo).find(".duration span").text(point.duration.text);
//	    		                    $(directionInfo).show();
//	    		                } else {
////	    		                    alert('Could not display directions due to: ' + status);
//	    		                }
//	    		            });
//    		            };
//    		            
//    		            var markerImage = new google.maps.MarkerImage('/resources/img/web/marker.png',
//										    		                    new google.maps.Size(353, 90),
//										    		                    new google.maps.Point(0, 0),
//										    		                    new google.maps.Point(30, 90)
//    		            											  );
//    		            
//    		            var marker = new google.maps.Marker({
//    		                position: markerPosition,
//    		                map: map,
//    		                icon: markerImage,
//    		                animation: google.maps.Animation.DROP,
////    		                title: $(mapElement).attr('data-description')
//    		            });
//    		            
//    		            google.maps.event.addListener(marker, 'click', function() {
//    		                infowindow.open(map, marker);
//    		            });    		        
//    		    	}  		    
//    		    });
//	        }   	    
//	    	loadMap();
//	    	
////	        $(window).resize(function() {
////	        	loadMap();
////	        });
//	    }
//    },
//    
//    initFancyBox: function() {
//    	$(".sys-image").fancybox({
//			scrolling: "no",
//			autoResize: false,
//    		maxWidth: 950,
//    		maxHeight: $(window).height() - 130,
//    		fitToView: false,
//    		width: '950px',
//    		height: 'auto',
//    		autoSize: false,
//    		closeClick: false,
//    		openEffect: 'none',
//    		closeEffect: 'none',
//    		padding: 0,
//    		helpers: {
//    			overlay: {
//    				locked: true
//    			}
//    		}
//        });
//    },
//    
//    initScrollPanel: function(panel, settings) {
//    	var animateCount = settings.animateCount ? settings.animateCount : 1;
//		var fullScreen = settings.fullScreen;
//    	var countScroll = settings.countScroll ? settings.countScroll : 1;
//		var container = panel.find(settings.container);
//		var scrollPrev = panel.find(settings.scrollPrev);
//		var scrollNext = panel.find(settings.scrollNext);
//		var elements = container.find(settings.element);
//		var horizontal = settings.horizontal != null ? settings.horizontal : true; 
//		var round = settings.round != null ? settings.round : true;
//		
//		var getSize = function(e) {
//			var size;
//			if (e) {
//				if(horizontal) {
//					size = e.getBoundingClientRect().width;
//				} else {
//					size = e.getBoundingClientRect().height;
//				}
//			}
//			return size;
//		};
//		var sizeElement = function() {
//			console.log("sizeElement=" + getSize(elements[0]));
//			return getSize(elements[0]);
//		};
//		var sizeFull = function() {
//			return sizeElement() * elements.length;
//		};
//		var sizeContainer = function() {
//			console.log("container=" + getSize(container[0]));
//			return getSize(container[0]);
//		};
//		
//		var calcSizeContainer = function() {
//			if(container.is(":visible")) {
//				scrollPrev.removeClass("hidden");
//				scrollNext.removeClass("hidden");
//				if(!round) {
//					scrollPrev.addClass("hidden");
//				}
//				
//				if(sizeFull()) {
//					if (sizeFull() <= sizeContainer()) {
//						scrollPrev.addClass("hidden");
//						scrollNext.addClass("hidden");
//					}
//					
//					elements.css(horizontal ? "left" : "top", 0);
//					return true;
//				}
//			}
//			return false;
//		};
//		var success = calcSizeContainer();
//		if(!success) {
//			panel.oneTime(300, function() {
//				calcSizeContainer();
//			});
//		}
//		
//		$(window).resize( function() {
//			elements.find("img").css("width", "auto");
//			calcSizeContainer();
//			elements.find("img").css("width", "100%");
//		});
//		
//		elements.css("position", "relative");
//		
//		var getLeftTop = function() {
//			return horizontal ? "left" : "top";
//		};
//		var getScrollParam = function(value) {
//			return horizontal ? {"left" : value} : {"top" : value};
//		};
//		
//		var getPrevMaxCountScroll = function() {
//			var offset = parseInt(elements.first().css(getLeftTop()).replace("px", ""));
//			if(isNaN(offset)) {
//				offset = 0;
//			}
//			
//			var maxCountScroll = countScroll;
//			while(maxCountScroll > 0) {
//				if(offset + sizeElement() * maxCountScroll <= 0) {
//					break;
//				}
//				maxCountScroll--;
//			}
//			return maxCountScroll;
//		};
//		var getNextMaxCountScroll = function() {
//			var offset = parseInt(elements.first().css(getLeftTop()).replace("px", ""));
//			if(isNaN(offset)) {
//				offset = 0;
//			}
//			
//			var maxCountScroll = countScroll;
//			while(maxCountScroll > 0) {
//				if(sizeElement() * maxCountScroll - offset <= sizeFull() - sizeContainer()) {
//					break;
//				}
//				maxCountScroll--;
//			}
//			return maxCountScroll;
//		};
//		var animateScroll = function(param) {
//			var currentIndex = Math.round(elements.first().position().left / sizeElement());			
//			var from = -currentIndex - animateCount;
//			if (from < 0) from = 0;
//			var to = -currentIndex + animateCount + 1;
//			var elementsAnimate = elements.slice(from, to);
//			elementsAnimate.animate(param, {
//				duration: 200		
//			});
//			elementsAnimate.promise().done(function() {
//				elements.css("left", parseFloat(elementsAnimate.first().css("left")));
//				scrollPrev.removeClass("hidden");
//				scrollNext.removeClass("hidden");
//				if(!round) {
//					if(getPrevMaxCountScroll() == 0) {
//						scrollPrev.addClass("hidden");
//					}
//					if(getNextMaxCountScroll() == 0) {
//						scrollNext.addClass("hidden");
//					}
//				}
//				runingScroll = false;
//			});
//		}
//		
//		var runingScroll = false;
//		scrollPrev.unbind("click").click(function() {
//			if(runingScroll) return false;
//			
//			var maxCountScroll = getPrevMaxCountScroll();
//			if(maxCountScroll == 0) {
//				if(round) {
//					container.prepend(elements.last());
//					elements = container.find(settings.element);
//					elements.css(getScrollParam("-=" + sizeElement() + "px"));
//					maxCountScroll = getPrevMaxCountScroll();
//				} else {
//					return false;
//				}
//			}
//			
//			runingScroll = true;
//			var param = getScrollParam("+=" + sizeElement() * maxCountScroll + "px");
//			
//			animateScroll(param);
//			
//			if(typeof settings.onScrollPrev == "function") {
//				settings.onScrollPrev();
//			}
//			return false;
//		});
//		scrollNext.unbind("click").click(function() {
//			if(runingScroll) return false;;
//			
//			var maxCountScroll = getNextMaxCountScroll();
//			if(maxCountScroll == 0) {
//				if (round) {
//					container.append(elements.first());
//					elements = container.find(settings.element);
//					elements.css(getScrollParam("+=" + sizeElement() + "px"));
//					maxCountScroll = getNextMaxCountScroll();
//				} else {
//					return false;
//				}
//			}
//			
//			runingScroll = true;
//			var param = getScrollParam("-=" + sizeElement() * maxCountScroll + "px");
//			
//			animateScroll(param);
//			
//			if(typeof settings.onScrollNext == "function") {
//				settings.onScrollNext();
//			}
//			return false;
//		});
//		
//		var scollTo = function(selector) {
//			if(runingScroll) return false;;
//			
//			var element = container.find(selector);
//			
//			var offset = parseInt(element.css(getLeftTop()).replace("px", ""));
//			if(isNaN(offset)) {
//				offset = 0;
//			}
//			
//			var i = 0;
//			while(i < elements.length && elements[i] != element[0]) i++;
//			
//			var offsetScroll = offset + i * sizeElement();
//			console.log("offsetScroll = " + offsetScroll + " offset = " + offset + " i * size = " + (i * sizeElement()));
//			
//			runingScroll = true;
//			var param = getScrollParam("-=" + offsetScroll + "px");
//			elements.animate(param, {
//				duration: 300		
//			});
//			elements.promise().done(function() {
//				if(!round) {
//					scrollPrev.removeClass("hidden");
//					scrollNext.removeClass("hidden");
//				
//					if(getPrevMaxCountScroll() == 0) {
//						scrollPrev.addClass("hidden");
//					}
//					if(getNextMaxCountScroll() == 0) {
//						scrollNext.addClass("hidden");
//					}
//				}
//				runingScroll = false;
//			});
//		};
//		return scollTo;
//	},
};