﻿var E = {
		
	
    initBase: function() {
    	$('input.datepicker').datepicker({ dateFormat: 'dd.mm.yy' });
    	$('input.timepicker').timepicker({
    		defaultTime: false,
    		showMeridian: false,
    		minuteStep: 5
    	});
    	
    	tinymce.init({
    		selector: '.wysiwyg',
    		height: 300,
    		fontsize_formats: "8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px",
	  		lineheight_formats: "8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px",
	  		block_formats: 'Paragraph=div; Header 1=h1; Header 2=h2; Header 3=h3; Header 4=h4; Header 5=h5; Header 6=h6',
	  		
	  		menubar: false,
	  		toolbar1: 'fontsizeselect | formatselect | lineheightselect | hr',
	  		toolbar2: 'undo redo | bold italic underline removeformat | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist outdent indent | link | fullscreen code',
	  		
			textcolor_map : [ 
				"000000", "Black", 
				"993300", "Burnt orange", 
				"333300", "Dark olive", 
				"003300", "Dark green", 
				"003366", "Dark azure", 
				"000080", "Navy Blue", 
				"333399", "Indigo", 
				"333333", "Very dark gray", 
				"800000", "Maroon", 
				"FF6600", "Orange", 
				"808000", "Olive", 
				"008000", "Green",
				"008080", "Teal", 
				"0000FF", "Blue", 
				"666699", "Grayish blue", 
				"808080", "Gray", 
				"FF0000", "Red",
				"FF9900", "Amber", 
				"99CC00", "Yellow green",
				"339966", "Sea green", 
				"33CCCC", "Turquoise",
				"3366FF", "Royal blue", 
				"800080", "Purple",
				"999999", "Medium gray", 
				"FF00FF", "Magenta",
				"FFCC00", "Gold", 
				"FFFF00", "Yellow", 
				"00FF00", "Lime", 
				"00FFFF", "Aqua", 
				"00CCFF", "Sky blue",
				"993366", "Red violet", 
				"FFFFFF", "White",
				"FF99CC", "Pink",
				"FFCC99", "Peach", 
				"FFFF99", "Light yellow", 
				"CCFFCC", "Pale green", 
				"CCFFFF", "Pale cyan", 
				"99CCFF", "Light sky blue", 
				"CC99FF", "Plum" 
			],
	  		
	  		plugins: [
				'link searchreplace code paste textcolor fullscreen hr lineheight'
	    	],
	  		content_css: [
	  		    'http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700',
	  			'/resources/css/admin.css'
			]
    	});
		
		$("input").first().focus();
    },
    
    initList: function() {
		var area = $(".sys-list");
		
		C.initSortable(area);		
		C.initRemoveable(area.find(".sys-item"));
	},
	
	initCarWashPage : function() {
		var area = $(".carwash-editor");

		E.initImagePanel(area.find(".carwash-image"), {
			imagesCrop : [ {
				container : ".image",
				cropWidth : 258,
				cropHeight : 207
			} ]
		});
		
		E.initImagePanel(area.find(".content-image"), {
			imagesCrop: [{
				container: ".image",	
				cropWidth: 0,
				cropHeight: 0,
				maxWidth: 0,
				maxHeight: 0,
			}],
			isMultiple: true,
			notNeedCrop: true
		});
	},
	
	initCarServicePage : function() {
		var area = $(".carservice-editor");

		E.initImagePanel(area.find(".carservice-image"), {
			imagesCrop : [ {
				container : ".image",
				cropWidth : 258,
				cropHeight : 207
			} ]
		});
		
		E.initImagePanel(area.find(".content-image"), {
			imagesCrop: [{
				container: ".image",	
				cropWidth: 0,
				cropHeight: 0,
				maxWidth: 0,
				maxHeight: 0,
			}],
			isMultiple: true,
			notNeedCrop: true
		});
	},
	
	initNewsPage : function() {
		var area = $(".news-editor");

		E.initImagePanel(area.find(".news-image"), {
			imagesCrop : [ {
				container : ".image",
				cropWidth : 258,
				cropHeight : 207
			} ]
		});
		
		E.initImagePanel(area.find(".content-image"), {
			imagesCrop: [{
				container: ".image",	
				cropWidth: 0,
				cropHeight: 0,
				maxWidth: 0,
				maxHeight: 0,
			}],
			isMultiple: true,
			notNeedCrop: true
		});
	},
	
	initContentPageEntity : function() {
		var area = $(".content-page-entity");

		E.initImagePanel(area.find(".content-image"), {
			imagesCrop: [{
				container: ".image",	
				cropWidth: 0,
				cropHeight: 0,
				maxWidth: 0,
				maxHeight: 0,
			}],
			isMultiple: true,
			notNeedCrop: true
		});
	},
//    
//    initPage: function() {
//    	var area = $(".page-editor");
//    	
//    	E.initImagePanel(area.find(".page-about-image"), {
//			imagesCrop: [{
//				container: ".image",
//				cropWidth: 420,
//                cropHeight: 580
//			}]
//		});
//    	
//    	E.initImagePanel(area.find(".page-images-panel"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 420,
//				cropHeight: 287
//			}],
//			isMultiple: true
//		});
//    	
//    },
//    
//    initSettingsPage: function() {
//    	var area = $(".settings-editor");
//    	
//    	E.initImagePanel(area.find(".settings-menu-image"), {
//			imagesCrop: [{
//				container: ".image",
//				cropWidth: 0,
//                cropHeight: 0
//			}]
//		});
//    },
//
//    initGalleryList: function() {
//		var area = $(".sys-galleries");
//		
//		C.initSortable(area);		
//		C.initRemoveable(area.find(".sys-item"));
//	},
//	
//	initGalleryPage: function() {
//		var area = $(".gallery-editor");
//		
//		C.initSortable(area);
//		
//    	E.initImagePanel(area.find(".gallery-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 358,
//                cropHeight: 250
//			}]
//		});
//		E.initImagePanel(area.find(".content-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 250,
//				cropHeight: 250
//			}],
//			isMultiple: true,
//			notNeedCrop: true
//		});
//	},    
//    
//	initMainImageList: function() {
//		var area = $(".sys-main_images");
//
//		C.initSortable(area);		
//		C.initRemoveable(area.find(".sys-item"));
//	},
//	
//	initMainImagePage: function() {
//		var area = $(".main-image-editor");
//
//    	var type = area.find(".main-image input[name=type]").val();
//    	var width = 0;
//    	var height = 0;
//    	if(type == "MAIN_IMAGE_SLIDER") {
//    		width = 1100;
//    		height = 450;
//    	} else if(type == "MAIN_IMAGE_PHOTO") {
//    		width = 550;
//    		height = 550;
//    	}
//    	
//		E.initImagePanel(area.find(".main-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: width,
//                cropHeight: height
//			}],
//		});
//	},
//	
//    initDishList: function() {
//		var area = $(".sys-dishes");
//		
//		area.on("click", ".sys-category", function() {
//			var id = $(this).attr("cid");
//			
//			var filterForm = area.find("form.sys-filter");
//			filterForm.find("input[name=categoryId]").val(id ? id : '');
//			filterForm.submit();
//			
//			return false;
//		});
//		
//		C.initSortable(area);		
//		C.initRemoveable(area.find(".sys-item"));
//	},
//	
//	initDishPage: function() {
//		var area = $(".dish-editor");
//		
//		C.initAutocomplete($("input[name*=categories]"), { 'multiple' : true, 'type' : 'category', formatResult: E.formatResultCategory});
//		
//    	E.initImagePanel(area.find(".dish-image"), {
//			imagesCrop: [{
//				container: ".image",
//				cropWidth: 0,
//                cropHeight: 0
//			}]
//		});
//	},
//	
//    initPosterList: function() {
//		var area = $(".sys-posters");
//		
//		C.initSortable(area);		
//		C.initRemoveable(area.find(".sys-item"));
//	},
//	
//	initPosterPage: function() {
//		var area = $(".poster-editor");
//		
//    	E.initImagePanel(area.find(".poster-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 706,
//                cropHeight: 1000
//			}]
//		});
//	},
//	
//    initArticleList: function() {
//		var area = $(".sys-articles");
//		
//		C.initRemoveable(area.find(".sys-item"));
//	},
//	
//	initArticlePage: function() {
//		var area = $(".article-editor");
//		
//		E.initImagePanel(area.find(".content-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 0,
//				cropHeight: 0,
//				maxWidth: 0,
//				maxHeight: 0,
//			}],
//			isMultiple: true,
//			notNeedCrop: true
//		});
//		
//    	E.initImagePanel(area.find(".article-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 510,
//                cropHeight: 400
//			}]
//		});
//    	
//    	E.initImagePanel(area.find(".article-small-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 283,
//                cropHeight: 400
//			}]
//		});
//    	
//    	E.initImagePanel(area.find(".article-inner-preview-image"), {
//			imagesCrop: [{
//				container: ".image",	
//				cropWidth: 1920,
//                cropHeight: 380
//			}]
//		});
//    	
//		var fillBlock = function(block, position) {
//			block.find("input[name*=videoId]").attr("name", "videoId" + position);
//			block.find("input[name*=link]").attr("name", "link" + position);
//		};
//    	E.initAddBlockPanel($(".videos"), fillBlock);
//    	
//    	$(".videos button[name=save]").click(function(e){
//    		e.preventDefault();
//    		var params = Util.serialize($(this).parents("form"));
//    		Util.postOnAjax(params + "&action=save-videos");
//    	});
//	},
//	
//	formatResultCategory: function(object) {
//		var level = object.value;
//		var text = object.text;
//		if (level < 1) {
//			return "<b>" + text + "</b>";
//		} else if (level < 2) {
//			return "<ins style='padding-left: 30px;'>" + text + "</ins>";
//		} else {
//			return "<div style='padding-left: 60px;'>" + text + "</div>";
//		}
//	},
	
	initImagePanel: function(area, params) {
		var isMultiple = false;
		var notNeedCrop = false;
		var imagesCrop = new Array({
			imageCrop : ".crop-image",
			maxWidth : 0,
			maxHeight : 0,
			cropWidth : 0,
			cropHeight : 0
		});
		var refresh; 
		if(params) {
			if(params.imagesCrop) {
				imagesCrop = params.imagesCrop;
			}
			if(params.isMultiple) {
				isMultiple = params.isMultiple;
			}
			if(params.notNeedCrop) {
				notNeedCrop = params.notNeedCrop;
			}
			if(params.refresh) {
				refresh = params.refresh;
			}
		}
		
		var imagePanel = area.hasClass("image-panel") ? area : area.find(".image-panel");
		var type = area.find("input[name=type]").val();
		var mode = area.find("input[name=mode]").val();
		var format = area.find("input[name=format]").val();
		var id = area.find("input[name=id]").val();
		var language = area.find("input[name=language]").val();
		
		var initListImages = function() {
			area.find(".list-images").SuperBox();
		}; 
		initListImages();
		
		if(!refresh) {
			refresh = function() {
				var clear = function() {
					imagePanel.find("input[name=imageUrl]").val("");
					imagePanel.find("input[type=file]").val("");
	//				save isEncrease value on multiple file upload requests
	//				imagePanel.find("input[name='isEncrease']:checked").attr("checked", false);
					imagePanel.find("input[type=file]").change();
				};
				if(area.find(".crop-image-panel").length) {
					Util.postOnAjax({action: "refresh-crop-image", type: type}, function() {
						clear();
					});
				}
				if(area.find(".list-images").length) {
					Util.postOnAjax({action: "refresh-image", type: type}, function() {
						clear();
						initListImages();
					});
				}
			};
		}
		
		var cropImage = function(srcImage, fileName) {
			var eventsArr = new Array();
			$.each(imagesCrop, function(i, imageCrop) {
				var containerSelector = imageCrop.container ? imageCrop.container : ".image";
				
				var imageEl = imagePanel.find(containerSelector);
				if(imageEl.hasClass("croping")) {
					imageEl.attr("style", "");
					imageEl.html("<img class='crop-image'/>");
				} else {
					imageEl.addClass("croping");
				}
				imageEl.find("img.crop-image").attr("src", srcImage);
				
				eventsArr[eventsArr.length] = C.initEditImage(imagePanel, {
					preview: [],
					maxWidth: imageCrop.maxWidth ? imageCrop.maxWidth : 300,
					maxHeight: imageCrop.maxHeight ? imageCrop.maxHeight : 300,
					cropWidth: imageCrop.cropWidth,
					cropHeight: imageCrop.cropHeight,
					cropImageClass: containerSelector,
					setImage: true
				});
			});
			
			imagePanel.find(".btn.save-image").show().unbind("click").click(function() {
				var args = {type: type, format: format, id: id, language: language, "tmpFile": fileName};
				
				$.each(eventsArr, function(i, events) {
					var cropData = events.getCropData();
					args["x" + (i > 0 ? i + 1 : "")] = cropData.x;
					args["y" + (i > 0 ? i + 1 : "")] = cropData.y;
					args["width" + (i > 0 ? i + 1 : "")] = cropData.width;
					args["height" + (i > 0 ? i + 1 : "")] = cropData.height;
				})
				$(document).oneTime(100, function(){
		    		Util.showLoadPanel();
				});
				Util._postBaseUrl("/admin/upload_image/save", args, function(response) {
					Util.closeLoadPanel();
					refresh();
		        });
				
				return false;
			});
		};
		
		var datas = new Array();
		imagePanel.find('#upload').fileupload({
			url: '/admin/upload_image/',
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		    sequentialUploads: true,
	        dataType: 'json',
	        autoUpload: false,
	        formData: function() {
	        	var	isEncrease = imagePanel.find("input[name='isEncrease']:checked").length > 0;
	        				
	            return [ {name: "mode", value: mode}, 
	            		 {name: "type", value: type},
	            		 {name: "id", value: id},
	            		 {name: "language", value: language},
	            		 {name: "isEncrease", value: isEncrease},
	            		 {name: "format", value: format} ];
	        },
	        done: function (e, data) {
	        	doneCallback(data);
	        },
	        change: function (e, data) {
	        	var names = new Array();
	            $.each(data.files, function (index, file) {
	            	names[names.length] = file.name;
	            });
	            imagePanel.find(".input-file input[type=text]").val(names.join(", "));
	            datas = [];
	        },
		    add: function (e, data) {
		    	datas[datas.length] = data;
		    }
		});
		
		var doneCallback = function(data) {
			Util.closeLoadPanel();
			var response = data;
			if(data.result) {
				response = data.result;
			} 
	        
	        if(response.status == 'size-error') {
	        	$.SmartMessageBox({
					title : "Ошибка",
					content : "Размер не подходит",
					buttons : '[Ok]'
				});
	        	return;
	        }
	        if(notNeedCrop) {
	        	refresh();
	        } else {
	        	cropImage(response.data.path, response.data.fileName);
	        }
	    };
		
		imagePanel.find(".btn.upload").off("click").on("click", function() {
			if(datas.length > 0) {
				Util.showLoadPanel();
			
				$.each(datas, function(i, v) {
					v.submit();
				});
				datas = [];
			}
			var imageUrl = imagePanel.find("input[name=imageUrl]");
			if(imageUrl.val() != "") {
				var isEncrease = imagePanel.find("input[name='isEncrease']:checked").length > 0;
				Util._postBaseUrl("/admin/upload_image/", {mode: mode, type: type, id: id, language: language,
														imageUrl: imageUrl.val(), isEncrease: isEncrease}, function(response) {
					doneCallback(response);
		        });
			}
			return false;
		});
		
		imagePanel.on("click", ".remove-image", function() {
			var self = $(this);
			var imageType = area.find(".superbox-show").prev().attr("imageType");
			if(!imageType) {
				imageType = type;
			}
			var removeId = self.attr("iid");
			if(!removeId) {
				if(isMultiple) {
					removeId = area.find(".block-image.active").attr("iid");
					if(!removeId) {
						removeId = area.find(".superbox-show").prev().attr("iid");
					}
				} else {
					removeId = id;
				}
			}
			Util._postBaseUrl('/admin/upload_image/remove', {mode: "mode_remove", type: imageType, format: format, id: removeId, language: language}, function() {
				if(isMultiple) {
					Util.reloadCurrentPage();
				} else {
					refresh();
				}
			});
			return false;
		});
		
		if(isMultiple) {
			imagePanel.on("click", ".edit-image", function() {
				var self = $(this);
				var id = area.find(".block-image.active").attr("iid");
				Util.post(self.parents("form").serialize() + "&action=edit-image&id=" + id, function() {
					Util.reloadCurrentPage();
				});
				return false;
			});
		}
	},
	
	initAddBlockPanel: function(panel, fillBlock, initBlock, onRemove) {
		panel.find(".block:not(.new-block)").each(function(){
			var block = $(this);
			if (typeof(initBlock) == "function") {
				initBlock(block);
			}
		});
		panel.on("click", ".remove-block", function() {
			var row = $(this).parents(".block");
			var remove = function() {
				row.remove();
				
				var position = 0;
				panel.find(".block:not(.new-block)").each(function(){
					var block = $(this);
					fillBlock(block, position);
					position++;
				});
			};
			if(onRemove) {
				onRemove(row, remove);
			} else {
				remove();
			}
			
			return false;
		});
		panel.find(".wysiwyg, iframe").css("width", "100%");
		panel.find(".add-block a").click( function() {
			var newBlock = panel.find(".block.new-block").clone();
			newBlock.removeClass("new-block");
			
			var position = panel.find(".block:not(.new-block)").length;
			fillBlock(newBlock, position);
			panel.find(".block").last().after(newBlock);
			
			newBlock.find(".init-wysiwyg").each( function() {
				E.initWysiwyg($(this));
			});
			if (typeof(initBlock) == "function") {
				initBlock(newBlock);
			}
			
			panel.find(".wysiwyg, iframe").css("width", "100%");

			return false;
		});
	}
};